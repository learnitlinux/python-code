# When file is in same directory as the program file, the open() method just need the file name.
print(
    """
    Different types of modes:
    r - Read
    w - Write
    a - Append
    r+ - Read&Write
    w+ - Writing and reading (Overwrites existing files or creates a new file!.)
    """
)

print("Step 1:")
print("Reading content of a file.")
# When you use with statement with open function, you do not need to close the file at the end, because with would
# automatically close it for you.
with open("pivalue.txt") as file_object:
    contents = file_object.read()
print(contents)
print()

with open("pivalue.txt") as file_object:
    contents_read_lines = file_object.readlines()
print("Reading content as a list:", contents_read_lines)
print()

# Using the absolute path of the file.
print("Reading a file using its absolute path")
file_path = "/root/pivalue.txt"
with open(file_path) as file_object2:
    contents = file_object2.read()

print(contents)
print()

# Reading one line at a time using file name as a string in a variable(file_name).
# Here the file_name does not represent the file directly, but it represents
# the file name as string.

file_name = "pivalue.txt"
print("Reading one line at a time.")
with open(file_name) as file_object3:
    for line in file_object3:
        print(line.rstrip())
print()

# Reading one line at a time,
print("Reading one line at a time...")
with open("pivalue.txt") as file_object4:
    for line in file_object4.readlines():
        print(line)
print()

print("Step 2:")
print(
    "stripping the extra lines and spaces to keep the pi value in one line, "
    "using strip() method"
)
with open("pivalue.txt") as file_object5:
    pi_oneline = ""
    for line in file_object5.readlines():
        pi_oneline += line.strip()
    print(pi_oneline)
    print("Length of pi:", len(pi_oneline))
    print(
        "When python reads from a file, it interprets the text in the file as a string, "
        "If you need to convert that to float or int, we must use the float() and int()"
        "functions."
    )
    print(type(pi_oneline))
    pi_value = float(pi_oneline)
    print(type(pi_value))
    print("We can see the difference below...")
    print(pi_oneline * 2)
    print(pi_value * 2)
print()

print("Same code as above but, using the for loop out side of the 'with' block...")
with open("pivalue.txt") as file_object6:
    all_lines = file_object6.readlines()
    print(
        "Here if you see, the content of the text file is stored as a list, hence"
        "the type() is reported as 'list' and length is number of lines in the txt file."
    )
    print(type(all_lines))
    print(len(all_lines))
pi_result = ""
for each_line in all_lines:
    pi_result += each_line.strip()
print(pi_result)
print()

print("Practicing the r,w,a modes...")

print(
    """Here we are using the r+ mode for read and write, as we are reading the contents
once we have written in the program, but we can read the content from the linux
shell as well...then in that case 'w' is good enough...\n"""
)

with open("newfile.txt", "r+") as new_file_object:
    new_file_object.write("Hello World\n")
    contents = new_file_object.read()
print("content is:", contents)

with open("newfile.txt", "a") as new_file_object2:
    new_file_object2.write(
        "This will append the above line, rather than overwriting.\n"
    )
new_file_object2.close()
with open("newfile.txt", "r") as read_file:
    new_contents = read_file.read()
print(new_contents)
print()

# Emptying the file again...
with open("newfile.txt", "w") as file_write_object:
    file_write_object.write("")
file_write_object.close()

# ToDo: yet to be done.
# print("A simple program to find out the number of words in a file, it shows how ")

my_file = open("test.txt")
with open("test.txt", "r") as new_obj:
    reading_test_file = new_obj.read()
    print(reading_test_file)
    # The below 'seek' method reset the cursor to top again.
    new_obj.seek(0)
    reading_test_file_twice = new_obj.read()
    print(reading_test_file_twice)
