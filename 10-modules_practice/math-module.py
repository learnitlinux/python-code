import math


def number(a, b):
    """This method returns multiple math module-builtin values
    for a given number"""
    print("square root of given number is {}".format(math.sqrt(a)))
    print("exp value of given number is {}".format(math.exp(a)))
    c = math.radians(b)
    print("sin value of given number is {}".format(math.sin(c)))


number(16, 90)
print()
print("Document is \n {}".format(number.__doc__))
