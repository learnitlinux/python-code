# This program is same as the 0-json-module-practice, but with try and except block and then
# refactored to a method named user_info.
# ToDo: We can even refactor it further into multiple functions, and describing each function
# purpose, and either retrieve the user data or ask for the user data, page 207 has more info

import json

import sh

user_data_file = "user_data.json"


def user_info():
    """Retrieve user info from a json format file, if file not found then, it will ask for
    user details and store it in the json format file."""
    try:
        with open(user_data_file) as f:
            data_load = json.load(f)

    except FileNotFoundError:
        username = input("Enter your use name: ")
        age = input("Enter your age: ")
        city = input("Enter your city: ")
        user_information = {
            "usrename": username.capitalize(),
            "age": age,
            "city": city.capitalize(),
        }

        with open(user_data_file, "w") as g:
            json.dump(user_information, g)
        print("Your data has been saved to file 'user_data.json'")
    else:
        print("User data:", data_load, "\n")


print(user_info.__doc__, "\n")
user_info()

# ToDo: future scope, if different user runs this program, it should ask to retrieve the
# information from the user.

# correct_user = input("Please Enter 'yes' or 'no' if the user details are correct or not: ")
# if correct_user.upper() == 'YES':
#     print("Your details are stored")
# elif correct_user.upper() == 'NO':
#     user_information = {}
#     with open(user_data_file, 'w') as file_obj:
#         json.dump(user_information, file_obj)
#     user_info()
# else:
#     print("Please enter 'yes' or 'no'")

# print("******If file 'user_data.json' exits, remove it and try again ******")
