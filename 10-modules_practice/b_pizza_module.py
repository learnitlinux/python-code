def build_pizza(name, *pizza_items):
    """
    function to Build a pizza with list of toppings.
    :param name: name of the pizza
    :param pizza_items: toppings
    :return: None
    """
    print("Name of the pizza: ", name)
    print("Your toppings:")
    for topping in pizza_items:
        print(" -", topping)
