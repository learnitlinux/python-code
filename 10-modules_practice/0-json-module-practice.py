print(
    """The json module allow you to dump simple Python data structures into a file and load
the data from that file the next time the program runs. We can use the json to share data
between different python programs. Even better the json data format is not specific to
Python, hence we can share thd data between other programming languages as well.
    
It's useful and portable format and easy to learn.
    """
)

# json.dump takes two arguments: a piece of data to store and a file object it can use to store
# data.

import json

print("Step 1")
numbers = [1, 2, 3, 4, 5]
# It is a customary to use the file extention as .json to indicate that the data in the file
# is stored in the JSON format.

file_name = "numbers.json"

with open(file_name, "w") as f:
    json.dump(numbers, f)

print("Step 2")
# Now, the below program uses json.load() to read the list back into memory.

file_name_2 = "numbers.json"
with open(file_name_2) as g:
    numbers_list = json.load(g)

print(numbers_list)

# The above example shows the simple way to share the data between two programs.

# Sample program to store user information.
username = input("Enter your use name: ")
age = input("Enter your age: ")
city = input("Enter your city: ")
user_information = {"usrename": username, "age": age, "city": city}

user_file_name = "user_info.json"
with open(user_file_name, "w") as file_object:
    json.dump(user_information, file_object)

with open(user_file_name) as file_object_2:
    print("User information:\n", json.load(file_object_2))
