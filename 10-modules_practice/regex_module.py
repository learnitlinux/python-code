import re

test_string = "123abc456789abc123ABC.new"
given_pattern = re.compile(r"abc")  # 'r' here represents the raw string
my_match = given_pattern.finditer(test_string)

print("finditer() method:")  # returns memory address
for each in my_match:
    print(each)
    print(each.span(), each.start(), each.end())
    print(each.group())  # just returns the match pattern
    print()

print("findall() method:")
# findall() #finds all matches
matches = given_pattern.findall(test_string)
print(matches)
print()

print("match() method:")
# match() - finds strings that begin with the match.
num_pattern = re.compile(r"123")
new_match = num_pattern.match(test_string)
print(new_match)
print()

print("search() method:")
# search() - finds first match
search_match = given_pattern.search(test_string)
print(search_match)
print()
# Meta Characters
# . ^ $ * + ? { } [ ] \ | ( )

# . Any character (except newline character) "he..o"
# ^ Starts with "^hello"
# \$ Ends with "world\$"
# * Zero or more occurrences "aix*"
# + One or more occurrences "aix+"
# { } Exactly the specified number of occurrences "al{2}"
# [] A set of characters "[a-m]"
# \ Signals a special sequence (can also be used to escape special characters) "\d"
# | Either or "falls|stays"
# ( ) Capture and group

print("'.' practice:")
# find . in a string, if not escaped, then it will match with each letter and print them
match_dot_pattern = re.compile(r"\.")

match = match_dot_pattern.finditer(test_string)
for each in match:
    print(each.span(), each.group())
print()

print("'$' practice:")
end_pattern = re.compile(r"new$")
match = end_pattern.finditer(test_string)
for each in match:
    print(each.span(), each.group())
print()

# More Metacharacters / Special Sequences¶
# A special sequence is a '\' followed by one of the characters in the list below, and has a special meaning:
#
# \d :Matches any decimal digit; this is equivalent to the class [0-9].
# \D : Matches any non-digit character; this is equivalent to the class [^0-9].
# \s : Matches any whitespace character;
# \S : Matches any non-whitespace character;
# \w : Matches any alphanumeric (word) character; this is equivalent to the class [a-zA-Z0-9_].
# \W : Matches any non-alphanumeric character; this is equivalent to the class [^a-zA-Z0-9_].
# \b Returns a match where the specified characters are at the beginning or at the end of a word r"\bain" r"ain\b"
# \B Returns a match where the specified characters are present, but NOT at the beginning (or at the end) of a word r"\Bain" r"ain\B"
# \A Returns a match if the specified characters are at the beginning of the string "\AThe"
# \Z Returns a match if the specified characters are at the end of the string "Spain\Z"


test_string = "hey1 12_ 123_"
# with + sign it will search 1, 12, 123 rather than 1,1,2,1,1,2,3
# with ? sign the _ is optional
number_pattern = re.compile(r"\d+_?")
digit_match = number_pattern.finditer(test_string)
min_3_occurrence = re.compile(r"\d{3}")
min_3_occurrence_match = min_3_occurrence.finditer(test_string)

test_string = "hey 1\t2\ns"
white_space_pattern = re.compile(r"\s")
white_space_match = white_space_pattern.finditer(test_string)

test_string = "\t tab"
word_pattern = re.compile(r"\w")
word_pattern_match = word_pattern.finditer(test_string)
non_word_pattern = re.compile(r"\W")
non_word_pattern_match = non_word_pattern.finditer(test_string)

test_string = "word here_word-n too_word, last word"
block_pattern = re.compile(r"\bword")
block_pattern_match = block_pattern.finditer(test_string)
non_block_pattern = re.compile(r"\Bword")
non_block_pattern_match = non_block_pattern.finditer(test_string)

test_string = "abcdz-qls-Tv-Vick218"
set_pattern = re.compile(r"[a-eVT1-7]")  # we can write back to back
set_pattern_match = set_pattern.finditer(test_string)

for match in [digit_match, min_3_occurrence_match, white_space_match, word_pattern_match, non_word_pattern_match,
              block_pattern_match,
              non_block_pattern_match, set_pattern_match]:
    for each in match:
        print(each)
    print("<---------------------------------------------------->")

go_dates = """2020-12-20
         2024-01-5
         01/04/1986
         1945/03/8 
         1990.05.31"""
dates_pattern = re.compile(r"\d{4}.\d{2}.\d+")  # here (.) dot matches any single char -, /, .
dates_find_all = dates_pattern.findall(go_dates)
print(dates_find_all)
print()

# grouping practices with ()
given_emails = """
                raju23@gmail.com
                ramu_joy@yahoo.de
                john.dev@gmail.in
                vinay.kota@msn.com
                zilker_456-notme@gmx.fr
                dumb-email_not@not.me
                dumb-2@GMAIL.com
                """
emails_pattern = re.compile(r"([a-zA-Z0-9-._]*)\@([a-z]*)\.(com|de|in|fr)")
email_matches = emails_pattern.finditer(given_emails)
for match in email_matches:
    print(match, "-", match.group(1), "-", match.group(2), "-", match.group(3))
print()

# split, sub

given_string = "In the usa, there are 50 states. One of the states is Texas and it is the biggest state in the usa."
split_pattern = re.compile(r"\bthe\b") # this is to match exact the not ...there are...
splitted_using_pattern = split_pattern.split(given_string)
print(splitted_using_pattern)

sub_pattern = re.compile(r"usa")
new_string = sub_pattern.sub("USA", given_string)
print(new_string)


given_urls = """
http://python.org
https://www.gmail.com
https://yahoo.com
http://www.python-learners.edu
"""

sub_pattern = re.compile(r"https?://(www\.)?([a-z-]+)(\.[a-z]+)")
sub_pattern_match = sub_pattern.finditer(given_urls)
for match in sub_pattern_match:
    print(match)

sub_pattern_match_replace = sub_pattern.sub(r"\2\3", given_urls)
print(sub_pattern_match_replace)