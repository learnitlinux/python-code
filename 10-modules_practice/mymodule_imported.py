# import mymodule # you can import as regular way or give an alias
import sys

from basics import mymodule as mm

mylist = ["hellO", "Hello", "hEllo", "hello", "HELLo"]
mm.search_module(mylist, "Hello")
print(mm.dummy_var)  # we can also call the variable

print(sys.path)

# sys.path.append(/path/to/directory) # this way we can append the new directory to search for the modules
