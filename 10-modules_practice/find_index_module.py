print("module-builtin is imported")
test = "test string, you can remove any time"


def find_index(list_to_search, string_to_search):
    """
    This method is to search for a string in a list and return the index, string_name
        :param list_to_search: give a list to search
        :param string_to_search: string in quotes to search
    """
    if not list_to_search == []:
        # global val
        for ind, val in enumerate(list_to_search):
            if string_to_search == val:
                print("Found it {},{}".format(ind, string_to_search))

        if string_to_search not in list_to_search:
            print('No string found "{}"'.format(string_to_search))
    else:
        print("You have supplied an empty string")
