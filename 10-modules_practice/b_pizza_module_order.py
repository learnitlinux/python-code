import b_pizza_module

""""
We can either import the whole module-builtin as below... 
import <<module_name>>
OR one or more functions of the module-builtin as below...
from module_name import func1, func2, func3
If you have imported a function then the dot notation as(module-builtin.funcxx) is not needed. We can
directly use the funcxx as we have explicitly imported the function.
"""
b_pizza_module.build_pizza(
    "veg spinach", "spinach", "cheese", "mushrooms", "olives", "bell peppers"
)
# ToDo: Yet to find out how to get the doc printed??
print(b_pizza_module.__doc__)
