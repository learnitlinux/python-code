__counter = 0
my_list = []


def my_own_main():
    print("Independent module")


def counter_func():
    global __counter
    __counter += 1
    return __counter


def sum_total(my_list):
    global __counter
    __counter += 1
    total = 0
    for each in my_list:
        total += each
    return "total={}, counter={}".format(total, __counter)


def multi_total(my_list):
    total = 1
    global __counter
    __counter += 1
    for each in my_list:
        total *= each
    return "total={}, counter={}".format(total, __counter)


if __name__ == "__main__":
    my_own_main()
else:
    print("You have loaded the my_module from a different program")
