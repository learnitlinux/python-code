import my_own_module

print("my counter is:", my_own_module.counter_func())
print("sum of a list: ", my_own_module.sum_total([1, 2, 3, 4, 5]))
print("sum of zeros: ", my_own_module.sum_total([0 for i in range(5)]))
print("multiple of a list of numbers: ", my_own_module.multi_total([1, 2, 3, 4, 5, 6]))
print("multiplication  ones: ", my_own_module.multi_total([1 for i in range(1)]))

if __name__ == "__main__":
    print("This line is from main module")
