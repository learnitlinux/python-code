import glob
import os

print(glob.__doc__)
abs_path = os.path.join(os.environ.get("HOME"), 'rpms')
os.chdir(abs_path)
print(glob.glob('*.rpm'))
print()

for package in glob.glob("*.rpm"):
    split_package_ext = os.path.splitext(package)
    package_without_ext = split_package_ext[0]
    print(package_without_ext)
