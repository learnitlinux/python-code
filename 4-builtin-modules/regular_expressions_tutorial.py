import re
my_text = "I am in Spain not in Germany"
pattern = "Spain"

# the pattern is not only matched but it also gives the index values like start and end. We can see those one by one
# below.
my_match = re.search(pattern, my_text)


print("Results of search pattern: ", my_match)
print("Results of index that search pattern span from and to:", my_match.span())
print("Results of starting index of a matched pattern:", my_match.start())
print("Results of ending index of a matched pattern:", my_match.end())
print()


# r.search(pattern, text) will match only once but to find all occurrences then we should use findall() method.
my_text = "I am in Spain not in Germany, but I like to visit Spain."
pattern = "Spain"

find_all_matches = re.findall(r"Spain", my_text)

print("Results of all the findings:", find_all_matches)
print("Group of all the findings:", my_match.group())
print("just a length of the list:", len(find_all_matches))

# Now, let's iterate through the matched pattern.
for each_matching in re.finditer(pattern, my_text):
    print(each_matching.span())

# Practice of Character Identifiers
# \A	Returns a match if the specified characters are at the beginning of the string	"\AThe"
# \b	Returns a match where the specified characters are at the beginning or at the end of a word
# (the "r" in the beginning is making sure that the string is being treated as a "raw string")	r"\bain"
# r"ain\b"
# \B	Returns a match where the specified characters are present, but NOT at the beginning (or at the end) of a word
# (the "r" in the beginning is making sure that the string is being treated as a "raw string")	r"\Bain"
# r"ain\B"
# \d	Returns a match where the string contains digits (numbers from 0-9)	"\d"
# \D	Returns a match where the string DOES NOT contain digits	"\D"
# \s	Returns a match where the string contains a white space character	"\s"
# \S	Returns a match where the string DOES NOT contain a white space character	"\S"
# \w	Returns a match where the string contains any word characters (characters from a to Z, digits from 0-9,
#       and the underscore _ character)	"\w"
# \W	Returns a match where the string DOES NOT contain any word characters	"\W"
# \Z	Returns a match if the specified characters are at the end of the string	"Spain\Z"

my_phone_no = "My phone number is 555-555-5555"
print("My phone no:",
      re.search("555-555-5555", my_phone_no).span())
# let's try the character identifier "\d"
# Here r"" represents the raw string means take it as it is and ignore the special meaning like new line for \n
# or tab for \t.

my_phone_no = "My phone number is 555-666-1234"
print("My phone no with char id:",
      re.search(r"\d\d\d-\d\d\d-\d\d\d\d", my_phone_no).span())


# Character	    Description	                Example Pattern Code	    Exammple Match
# +	            Occurs one or more times	Version\w-\w+	            Version A-b1_1
# {3}	        Occurs exactly 3 times	    \D{3}	                    abc
# {2,4}	        Occurs 2 to 4 times	        \d{2,4}	                    123
# {3,}	        Occurs 3 or more	        \w{3,}	                    anycharacters
# \*	        Occurs zero or more times	A\*B\*C*	                AAACC
# ?	            Once or none	            plurals?	plural


# with quantifier {n}
print("My phone no with quantifier:",
      re.search(r"\d{3}-\d{3}-\d{4}", my_phone_no).span())

# to grab the found number, use re.search().group
print("To grab the matched number:",
      re.search(r"\d{3}-\d{3}-\d{4}", my_phone_no).group())


phone_match = re.compile(r"(\d{3})-(\d{3})-(\d{4})")
search_in_match = re.search(phone_match, my_phone_no)
print("", search_in_match.group(1))




