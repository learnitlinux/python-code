import os
import sys
import subprocess as sub

print("Step 1")
print("getcwd: ", os.getcwd())
# listing the files under current dir
print(sub.call(["ls", "-l"]))


# Now, writing the 'ls' output to a file.
print(sub.call(["ls", "-l"], stdout=open("output.txt", "w")))
print(
    """returncode: Exit status of the child process. Typically, an exit status of 0 indicates that it ran 
successfully."""
)

with open("output.txt", "r") as output_file:
    output_file.read()

print(sub.call(["more", "/root/Documents/python-code/4-builtin-modules/output.txt"]))
