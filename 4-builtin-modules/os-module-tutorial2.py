import os
import pwd

print("Step 1")
print(os.getuid())
print(os.getpid())
print(os.getppid())
print(pwd.getpwall())
for some in pwd.getpwall():
    print(some[0:])
    # if you only need the users remove : above and you get the first item in tuple.
print()

print("Step 2")
print(os.name)
# POSIX (Portable Operating System Interface) is a set of standard operating system interfaces
# based on the Unix operating system. The need for standardization arose because enterprises
# using computers wanted to be able to develop programs that could be moved among different
# manufacturer's computer systems without having to be recoded. Unix was selected as the basis
# for a standard system interface partly because it was "manufacturer-neutral." However, several
# major versions of Unix existed so there was a need to develop a common denominator system.
print(os.environ)
type_is = os.environ

print(os.environ["PATH"])
print(os.environ["HOME"])
