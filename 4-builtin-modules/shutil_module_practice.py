import shutil
import os
# Remove files first, so that you are not reading from files that are created last time.
try:
    os.remove("/tmp/data1.txt")
    os.remove("/tmp/data2.txt")
    os.remove("/tmp/new_file.txt")
except FileNotFoundError as e:
    print('Files might not be there yet!, just ignore.')

# Write to a file called /tmp/data1.txt

message = """This data1 file is created by the shutil_module_practice.py script.
Please ignore.
"""
with open("/tmp/data1.txt", "w") as f:
    f.write(message)
    f.close()

# Copy the file using shutil
file1 = open("/tmp/data1.txt", "r")
file2 = open("/tmp/data2.txt", "w")
shutil.copyfileobj(file1, file2)
file1.close()
file2.close()

# Read the copied file.
print("- Output from /tmp/data2.txt:")
with open("/tmp/data2.txt", "r") as f:
    for line in f.readlines():
        print(line)

file1.close()
file2.close()

# another way to copy the data, simple.
shutil.copy("/tmp/data1.txt", "/tmp/new_file.txt")

print("- Output from /tmp/new_file.txt:")
with open("/tmp/new_file.txt", "r") as f:
    for line in f.readlines():
        print(line)


# ToDo: Some examples below might be missing the code, implement them when you have time..

# With shutil.copy2() we can copy the metadata like creation of the file...etc.
# shutil.copyfile()
# shutil.move()
# shutil.copytree()
# shutil.rmtree()





# cols, lines = shutil.get_terminal_size()
# print(cols)
# print(lines)
