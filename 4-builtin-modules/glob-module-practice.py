import os
import glob
user_home_dir = os.environ.get("HOME")
my_full_path = os.path.abspath(os.path.join(user_home_dir, 'keep-it'))
pattern = os.path.join(my_full_path, "*rpm")
# print(pattern) # optional to print

print("Step 1:")
list_of_all_rpms = glob.glob(pattern)
print(list_of_all_rpms)

# now only rpms which which are 32 bit version
print("Step 2:")
pattern = os.path.join(my_full_path, "[a-z]*i686.rpm")
list_of_32_bit_rpms = glob.glob(pattern)
print(list_of_32_bit_rpms)

print("Step 3:")
pattern = os.path.join(my_full_path, "**/*.rpm")
recursive_search_of_rpms = glob.glob(pattern, recursive=True)
print(recursive_search_of_rpms)

# Only enable to see all the config files in /etc/
# print(glob.glob('/etc/**/*conf', recursive=True))

