#!/usr/bin/python
import os
print(os)
# Better way
print(os.__file__) # useful information, to locate the location of the module-builtin python file
# print(os.__doc__)
# The below dir() function will print all the attributes and methods within the 'os' module-builtin,
# uncomment to see..!
# print(dir(os))

# we can also use r'/path/to/dir' feature to escape the special meaning of back slash '\', it will interpret the
# string as raw string...
# os.chdir(r'/tmp/dummy')
# How to get help on the available functions in a module.
# help(os.chdir)

current_dir = os.getcwd()
print("prints current working directory: " + current_dir)

# change directory to..!
os.chdir("/home/bharath")
print("\nNow, the current working directory is changed to: " + os.getcwd())

# If no arguments passed, it will list the current working directory list. Here I used ',' to separate as the
# os.listdir() will list and we can't combine a list with a string with '+' like I used above

print(
    "\nCurrent working directory:", os.getcwd(), "- files and folders: ", os.listdir()
)

# make directory with os.mkdir
print("\nos.mkdir creates a new directory /tmp/dummy:", os.mkdir("/tmp/dummy"))
print("\nos.makedirs creates sub directories /tmp/a/b/c:", os.makedirs("/tmp/a/b/c"))
print(
    "\nlisting the /tmpa and /tmp/b directory structures-",
    "in folder /tmp/a-",
    os.listdir("/tmp/a"),
    "in folder /tmp/a/b-",
    os.listdir("/tmp/a/b"),
)
print("\nchmod:", os.chmod('/tmp/a/b/c', 755))
print("chown:", os.chown("/tmp/a/b/c", 1000, 1000))

# Remove directories recursively. Works like rmdir() except that, if the leaf directory is successfully removed,

# removedirs() tries to successively remove every parent directory mentioned in path until an error is raised
# (which is ignored, because it generally means that a parent directory is not empty). For example,
# os.removedirs('foo/bar/baz') will first remove the directory 'foo/bar/baz', and then remove 'foo/bar' and 'foo'
# if they are empty. Raises OSError if the leaf directory could not be successfully removed.

print("\nRemove directory with rmdir:", os.rmdir("/tmp/dummy"))
os.chdir("/tmp")
print(
    '\nRemove directories and its content, USE IT WITH CAUTION BECAUSE IT REMOVES THE PARENT, HERE IT IS "a":',
    os.removedirs("a/b/c"),
)
# Try to list /tmp/a which is removed in above command will give us an error and we are catching and proceeding with
# try and except
try:
    os.listdir("/tmp/a")
except FileNotFoundError as e:
    print("\nHere:", e)

with open("/tmp/file.txt", "w") as myfile:
    myfile.write("Hello world \n")
    myfile.close()

# Not working try to find why?
with open("/tmp/file.txt", "r") as new_file:
    print("\nReading Here:", new_file.read())
    new_file.close()

# access(path,mode)
# os.F_OK  ? Found
# os.R_OK  ? Readable
# os.W_OK  ? Writable
# os.X_OK  ? Executable
print("found a file?", os.access("no-such-file.txt", os.F_OK))

# Renaming a file
print("os.rename to rename a file: ", os.rename("/tmp/file.txt", "/tmp/newfile.txt"))
# Stat of a file.
print("\nStat of a file:", os.stat("/tmp/newfile.txt"))
# You can call each of them by
print("\nst_mtime:", os.stat("/tmp/newfile.txt").st_mtime)


# clean up
os.remove("/tmp/newfile.txt")
# check of cleanup
try:
    os.listdir("/tmp/newfile.txt")
except FileNotFoundError as e:
    print("\nHere:", e)

# os.walk() method
my_dir = "/home/bharath/Desktop"
for dirpath, dir_names, file_names in os.walk(my_dir):
    print("\ndirpath:", dirpath)
    print("dir_names:", dir_names)
    print("file_names:", file_names)

# os.environ
print("\nMy HomeDir:", os.environ.get("HOME"))
print("My Shell:", os.environ.get("SHELL"))

# os.path
print(
    "\njoin a file with a path:",
    os.path.join(os.environ.get("HOME"), "no_such_file_exist.txt"),
)
print("check for existence:", os.path.exists("/home/bharath/no_such_file_exist.txt"))
print("is /home/bharath a dir?:", os.path.isdir("/home/bharath"))
print("is /etc/passwd a file?:", os.path.isfile("/etc/passwd"))
print(
    "basename of /tmp/no_dir/no_file.txt: ", os.path.basename("/tmp/no_dir/no_file.txt")
)
print("dirname of /tmp/no_dir/no_file.txt:", os.path.dirname("/tmp/no_dir/no_file.txt"))
# if we need both of those...
print("split:", os.path.split("/tmp/no_dir/no_file.txt"))
print("split text:", os.path.splitext("/tmp/no_dir/no_file.png"))
print()

# adds the os.getcwd() to the file and produce abspath.
print(os.getcwd())
print(os.path.abspath("dummy_file_does-Not_exist.txt"))
print(os.path.abspath(os.path.join(os.environ.get("HOME"), "a/b/no/none")))
print()

# print all os.path module
print(dir(os.path))
print()

# There are a couple of concerns here. First, to run the method, Python opens its own console. Using system()
# too much is inefficient and can hog resources. More importantly, system() can run anything. That means it can
# do some serious damage. If you're going to use system() use it sparingly and under controlled circumstances.
print(os.system("ps -ef  | grep bash | grep -v grep"))

# os.environ prints tuple of dictionaries of all environment variable. To print each environment variable
# we rotate through for loop like below.
# print(os.environ)
for key, val in os.environ.items():
    if key == "HOME":
        print("{:5} {}".format(key, val))
print()

print("Collecting the user, system, and process information")
print("Current user UID:", os.getuid())
print("OS name: ", os.name)
print("PID: ", os.getpid())
print("PPID: ", os.getppid())