import os
import shutil

try:
    os.makedirs("/tmp/some/folder/exist/here")
except FileExistsError as e:
    print("Ignore if exists")

os.chdir("/tmp")
try:
    os.rmdir('some')
except OSError as e:
    print(e)

shutil.rmtree('/tmp/some')