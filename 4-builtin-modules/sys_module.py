#!/usr/bin/python3.6
import sys

print("\nThe system platform: ", sys.platform.title())
# copy right of the python interpreter, enable only to see that.
# print("\nThe copyright of python", sys.copyright)
print("\npython version:", sys.version)

print("\nPython path", sys.path)
print("\nPython executable in our OS:", sys.executable)
print("Run the current script with some args as example below and enable the print line."
      "./4-builtin-modules/sys_module.py some args passed here")
# print("\n", sys.argv)
print(sys.stderr)