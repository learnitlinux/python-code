import math


def prime_number(number):
    if number == 0 or number == 1:
        print("0 and 1 are not prime numbers")
        return False
    count = 0
    divisor_list = []
    for item in range(2, int(math.sqrt(number)) + 1):
        if number % item == 0:
            divisor_list.append(item)
            count += 1
    if count >= 1:
        print("Not a prime number")
        print(divisor_list)
        return False
    else:
        print("It is a prime number")
        return True


prime_number(0)
prime_number(1)
prime_number(20)
prime_number(3)
prime_number(2)
prime_number(4)
prime_number(5)
