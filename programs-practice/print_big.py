def print_big(pattern):
    dict_pattern = {
        0: "  {0}  ".format(pattern),
        1: " {0} {0} ".format(pattern),
        2: "{0}{0}{0}{0}{0}".format(pattern),
        3: "{0}   {0}".format(pattern),
        4: "{0}   {0}".format(pattern),
    }
    for each_key in range(0, 5):
        print(dict_pattern[each_key])


print_big("k")


def print_big_two(letter):
    patterns = {
        1: "  *  ",
        2: " * * ",
        3: "*   *",
        4: "*****",
        5: "**** ",
        6: "   * ",
        7: " *   ",
        8: "*   * ",
        9: "*    ",
    }
    alphabet = {
        "A": [1, 2, 4, 3, 3],
        "B": [5, 3, 5, 3, 5],
        "C": [4, 9, 9, 9, 4],
        "D": [5, 3, 3, 3, 5],
        "E": [4, 9, 4, 9, 4],
    }
    for pattern in alphabet[letter.upper()]:
        print(patterns[pattern])


print_big_two("b")
