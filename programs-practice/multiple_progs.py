def sphere_volume_calc(radius):
    return (4 / 3) * (22 / 7) * radius**3


print(sphere_volume_calc(10))


def number_in_given_range(*args):
    return args[0] in range(args[1], args[2] + 1)


print(number_in_given_range(7, 2, 7))


def up_low(string):
    count_up = 0
    count_low = 0
    for letter in string:
        if letter in [",", ".", ":", " ", "!"]:
            continue
        if letter.isupper():
            count_up += 1
        elif letter.islower():
            count_low += 1
    print("Number of upper case characters: ", count_up)
    print("Number of lower characters: ", count_low)


up_low("Hello, John!.")
