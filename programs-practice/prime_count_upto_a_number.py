import math


def print_prime_numbers(number):
    if number == 0 or number == 1:
        print("0 and 1 are not prime numbers")
        return False
    prime_list = []
    for each_number in range(2, number + 1):
        count = 0
        for divisor_num in range(2, int(math.sqrt(each_number)) + 1):
            if each_number % divisor_num == 0:
                count += 1
        if count >= 1:
            pass
        else:
            prime_list.append(each_number)
    print(prime_list)


print_prime_numbers(0)
print_prime_numbers(1)
print_prime_numbers(2)
print_prime_numbers(3)
print_prime_numbers(4)
print_prime_numbers(20)
print_prime_numbers(23)
print_prime_numbers(203)
