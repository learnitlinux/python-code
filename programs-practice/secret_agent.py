# Returns if a '007' is found in a list.


def find_007(my_list):
    for index in range(len(my_list) - 2):
        if my_list[index : index + 3] == [0, 0, 7]:
            print("True")
            break


input_list = [0, 0, 7]
find_007(input_list)

input_list = [1, 0, 0, 7, 2]
find_007(input_list)

input_list = [1, 0, 0, 7]
find_007(input_list)
