def main():
    print("I'm in first_module main method: {}".format(__name__))


def child_method():
    print("Some method in the module fist_module.py")


print("I am in the main code, will print either run directly or imported.")

# __name__ is a "variable" and it will be set to __main__ if script is run directly.
# It means, is this file being run directly by python or being imported?.

if __name__ == "__main__":
    main()
else:
    print("first module is imported in this program.")
