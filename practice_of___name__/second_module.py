# The moment you start import the main block of the code will be run
print("The import starts here\n")
import first_module

print("\nEnds here\n")
first_module.child_method()


def main():
    print("I'm in second_module: {}".format(__name__))


if __name__ == "__main__":
    main()
