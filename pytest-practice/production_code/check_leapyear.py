def check_leap_year(year_number):
    try:
        if year_number % 100 == 0 and year_number % 400 == 0:
            return True
        elif year_number % 100 == 0 and year_number % 400 != 0:
            return False
        return year_number % 4 == 0

    except TypeError as e:
        return "You entered a non integer, please try again"
