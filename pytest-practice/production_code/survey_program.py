class LanguageSurvey:
    def __init__(self, lang_question):
        self.question = lang_question
        print(self.question)
        self.responses = []

    def receive_responses(self, each_response):
        self.responses.append(each_response.title())

    def show_responses(self):
        for each_response in self.responses:
            print("Language you like: {}".format(each_response))


if __name__ == "__main__":
    message = "Please enter your favourite languages, 'q' to quit: "
    new_survey = LanguageSurvey(message)
    while True:
        usr_resp = input("Language: ")
        if usr_resp == "q":
            break
        else:
            new_survey.receive_responses(usr_resp)
    print(new_survey.responses)
    print(new_survey.show_responses())
    print("MAIN PROGRAM", new_survey.responses)
