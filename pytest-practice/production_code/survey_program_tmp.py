class AnonSurvey:
    def __init__(self, responses):
        self.responses = responses

    def store_responses(self, new_response):
        self.responses = []
        new_response = str(new_response)
        if new_response == "q":
            exit()
        else:
            self.responses.append(new_response)

    def print_responses(self, responses):
        self.responses = responses
        for each_response in self.responses:
            print("{}".format(each_response).title())


print("What are all your favourite languages?")
list_items = ["english", "hindi"]
AnonSurvey(list_items)
AnonSurvey.print_responses(list_items)
