# from name_function import get_formatted_name


def get_formatted_name(first, last, middle=""):
    if middle:
        return "{} {} {}".format(first, middle, last).title()
    else:
        return "{} {}".format(first, last).title()


if __name__ == "__main__":
    first_name = input("Enter first name: ")
    middle_name = input("Enter middle name: ")
    last_name = input("Enter last name: ")
    full_name = get_formatted_name(first_name, last_name, middle_name)
    print(full_name)
