from collections import namedtuple

import pytest

Task1 = namedtuple("Task1", ["summary", "name", "done", "id"])
Task1.__new__.__defaults__ = (None, None, False, None)
# Task1.__new__.__defaults__ = ()


def test_access():
    t1 = Task1("buy milk", "Bharath", True, 100)
    assert t1.summary == "bu milk"


def test_no_value():
    t2 = Task1()
    assert t2.name is None


def test_asdict():
    t3 = Task1("go home", "raj", True, 200)
    t3_task = t3._asdict()
    print(t3_task)
    expected = {"summary": "go home", "name": "raj", "done": True, "id": 200}
    assert t3_task == expected


# @pytest-practice.mark.bharath_run
def test_replace():
    t4 = Task1("go home", "raj", True, 200)
    t4_after = t4._replace(name="Mahesh")
    assert t4_after == Task1("go home", "Mahesh", True, 200)
