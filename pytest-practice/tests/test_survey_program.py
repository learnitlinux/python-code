import unittest
from survey_program import LanguageSurvey

# Naming of the class should be related to the program you test and a word 'Test' in it


class SurveyTestProgram(unittest.TestCase):
    def setUp(self):
        question = "Enter your fav language:"
        self.my_survey = LanguageSurvey(question)

    def test_receive_single_resppnse_case(self):
        self.my_survey.receive_responses("hindi")
        self.assertIn("Hindi", self.my_survey.responses)

    def test_receive_responses_case(self):
        self.my_survey.receive_responses("hindi")
        self.my_survey.receive_responses("teLugu")
        self.assertNotIn("hindi", self.my_survey.responses)
        self.assertIn("Hindi", self.my_survey.responses)
        self.assertIn("Telugu", self.my_survey.responses)


if __name__ == "__main__":
    unittest.main()
