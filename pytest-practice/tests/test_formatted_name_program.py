import unittest
from name_program import get_formatted_name

# Naming of the class should be related to the program you test and a word 'Test' in it


class NameTest(unittest.TestCase):
    def test__get_formatted_name__first_last_names(self):
        full_name = get_formatted_name("bharath", "jakkani")
        self.assertEqual(full_name, "Bharath Jakkani")

    def test_get_formatted_name__first_middle_last_names(self):
        full_name_2 = get_formatted_name("bharath", "jakkani", "vyas")
        self.assertEqual(full_name_2, "Bharath Vyas Jakkani")


if __name__ == "__main__":
    unittest.main()
