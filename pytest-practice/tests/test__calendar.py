from production_code import check_leapyear


def test__calendar_happycase():
    result = check_leapyear.check_leap_year(19996)
    assert result == True


def test_calendar_divisible_by_100_and_400():
    result = check_leapyear.check_leap_year(2000)
    assert result == True


def test_calendar_not_divisible_by_400():
    result = check_leapyear.check_leap_year(1800)
    assert result == False


def test_calendar_odd_year():
    result = check_leapyear.check_leap_year(2015)
    assert result == False


def test_calendar_a_letter():
    result = check_leapyear.check_leap_year("abcd")
    print(result)
    assert "please try" in result
