#
# class Employee:
#     def __init__(self, first, last, id, salary):
#         self.first_name = first
#         self.last_name = last
#         self.id_no = id
#         self.salary = salary
#         self.email = str(self.first_name).lower() + '.' + str(self.last_name).lower() + 'example.com'
#
#     def fullname(self):
#         return str(self.first_name).capitalize() + ' ' + str(self.last_name).capitalize()
#
#     def email_id(self):
#         return str(self.first_name).lower() + '.' + str(self.last_name).lower() + 'example.com'
#
#
# emp1 = Employee('bharath', 'jakkani', 1234, 30000)
# print(emp1.fullname())
# print(emp1.email)
#
# print()
#
# emp1.first_name = 'Nikhila'
# print(emp1.fullname())
# print("Here the email id is still bhrath.jakkani ")
# print(emp1.email)
#
# print()
# print("Here the email_id changed to the nikhila.jakkani because the self.first_name was given as input to the"
#       "'email_id' method")
# print(emp1.email_id())
#
# print()
# print("But here we have to change the email attribute to email_id() method everywhere in the code where it was called."
#       "So, let's use the '@property' decorator"


print("######### Please use any one code at a time. ##########")


class NewEmployee:
    def __init__(self, fname, lname, age, id):
        self.first_name = fname
        self.last_name = lname
        self.age = age
        self.id = id
        self.email_id = self.first_name + "." + self.last_name + "@email.com"

    # @property
    def fullname(self):
        return (
            str(self.last_name).capitalize() + " " + str(self.first_name).capitalize()
        )


new_emp1 = NewEmployee("john", "obeory", 30, 1234)
print(new_emp1.fullname)
print(new_emp1.fullname())

new_emp1.first_name = "rose"
print(new_emp1.fullname)
print(new_emp1.fullname())
