class NewEmployee:
    raise_amount = 1.04  # class variable (4%raise)
    num_employees = 0

    def __init__(self, first, last, salary):
        # Instance variables are below which are started as self. and unique to each instance
        self.fname = first
        self.lname = last
        self.pay = salary
        self.email = first + "." + last + "@example.com"
        NewEmployee.num_employees += 1
        # As the __init__ method run each time an employee instance is created,
        # it make sense to increase the number by 1 each time.

    def fullname(self):
        return "{} {}".format(self.fname, self.lname)

    def pay_raise_with_classname(self):
        self.pay = self.pay * NewEmployee.raise_amount
        # When you are using the class variables in methods, you must use the classname.variable

    def pay_raise_with_instance(self):
        self.pay = self.pay * self.raise_amount


# In the below the __init__ method will be run and assign the variables.
new_emp1 = NewEmployee("Bharath", "Jakkani", 30000)
new_emp2 = NewEmployee("Nikhila", "Jakkani", 40000)

print(new_emp1.fname)
print(new_emp1.pay)

print()

print(new_emp2.fname)
print(new_emp2.pay)

print()
new_emp1.raise_amount = 1.05
print(
    "Here we have changed the raise amount to 5%, for employee1 instance only, and others are not affected"
)
print("employee1 raise amount", new_emp1.raise_amount)
print("employee2 raise amount", new_emp2.raise_amount)
print("NewEmployee raise amount", NewEmployee.raise_amount)

print(
    "\nHere I have called the employee1 pay_raise_with_classname method, which still takes 4% itself as the"
    "\nclassname.raise is till 4%"
)
new_emp1.pay_raise_with_classname()
print(new_emp1.pay)


print(
    "\nHere we are calling the employee1 pay_raise_with_instance method, which is changed to 5% above."
)
new_emp1.pay_raise_with_instance()
print(new_emp1.pay)

print(
    "The conclusion here is, always use the instance variables and pay_raise_with_instance like methods"
    "\nbecause they are instance specific. Also we can remove the pay_raise_with_classname method"
    "\nfrom this code once it is understood.\n"
)

print("Some more info....")
print(
    "Below we can see the raise_amount class variable is also listed for employee1 but not for employee2."
)
print(new_emp1.__dict__)
print(new_emp2.__dict__)

print("Number of employees: ", NewEmployee.num_employees)
new_emp3 = NewEmployee("john", "smith", 30000)
new_emp4 = NewEmployee("mary", "summers", 40000)
print(
    "Number of employees after initiating two more new employees new_emp3 and new_emp4 is: ",
    NewEmployee.num_employees,
)
