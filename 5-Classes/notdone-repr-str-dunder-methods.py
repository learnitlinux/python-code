class Employee:
    def __init__(self, firtname, lastname, id, pay):
        self.first = firtname
        self.last = lastname
        self.badge = id
        self.payment = pay

    def __repr__(self):
        return "Employee('{}', '{}', '{}')".format(self.first, self.last, self.badge)

    def __str__(self):
        return "{} - {} - {}".format(self.first, self.last, self.badge)

    def __add__(self, other):
        return self.payment + other.payment


emp1 = Employee("Bharath", "Jakkani", 1234, 90000)
emp2 = Employee("Nikhila", "Jakkani", 2345, 60000)
emp3 = Employee("John", "Smith", 2233, 50000)

print(emp1)

# We can use the existing methods like __add__ and __len__ to our class and make them behave the way we want.

# If I want to add two employees, salary I can do print(emp1.pay + emp2.pay) but it can also be achieved by implementing
# customized __add__ method and just ask the two instances to add the pay automatically.

# Known way.
print(emp1.payment + emp2.payment)

# using __add__ method but it is observed the __add__ method only works on two class objects here.
# ToDo: What if we have to add multiple users salaries
print(emp1 + emp3)
