class CarClass:
    """
    Override method and Instances as Attributes model class example:
    The current class defines a car make, model and year of manufacture. If no initial
    mileage is set then current mileage is set to default to "0". If the car drives
    for extra miles (the value must be >0) then those will be added to current mileage
    after we call the function add_mileage. If user tries to tamper the current miles
    with less than what it was driven (current_mileage + add_mileage) then it gives
    an "Error" message.

    Now, in this program we have implemented a subclass in the superclass called Electric Car.
    The methods in this subclass can only be called from subclass. We can override a method
    in the subclass driven from superclass, ex: gas_tank_capacity().

    We have created a new class Battery and assign that to an attribute of ElectricCar class.
    Now, we can call all the methods under Battery class using  dot notation.
    """

    def __init__(self, make, model, year):
        self.make = make
        self.model = model
        self.year = year
        self.mileage = 0
        self.default_tank_capacity_in_Ltr = 30

    def car_details(self):
        print(
            "\t Manufacturer: {}\n"
            "\t        Model: {}\n"
            "\t         Year: {}".format(self.make, self.model, self.year)
        )

    def current_mileage_reading(self, current_miles):
        if self.mileage > current_miles:
            print("Error: Cannot set a lower miles than already existing")
        else:
            self.mileage = current_miles
            print("  Current Mileage: {}".format(current_miles))

    def add_mileage(self, miles_added):
        if miles_added > 0:
            self.mileage += miles_added
            print("Added extra miles: {}".format(miles_added))
        else:
            print("Error: No negative values should be given")

    def final_mileage_reading(self):
        print("Final Mileage reading: {}".format(self.mileage))

    def gas_tank_capacity(self):
        print("Gas tank capacity: {}L".format(self.default_tank_capacity_in_Ltr))


# Created a Battery class separately from ElectricCarClass.


class Battery:
    def __init__(self):
        self.capacity_in_kw = 100
        self.size_in_qft = 12

    def battery_capacity_in_kw(self):
        print(" Battery Capacity: {}Kw".format(self.capacity_in_kw))

    def battery_size_in_qubic_ft(self):
        print("     Battery Size: {}qft".format(self.size_in_qft))


class ElectricCarClass(CarClass):
    def __init__(self, make, model, year):
        CarClass.__init__(self, make, model, year)
        # Here We added an attribute called self.battery, this line tells python to create a new
        # instance of a Battery and assign that instance to the attribute self.battery.
        self.battery = Battery()

    def gas_tank_capacity(self):
        print("Error: No Gas tank for Electric car")


print(CarClass.__doc__)
car_one = CarClass("Honda", "Civic", "2019")
car_one.car_details()
car_one.current_mileage_reading(25000)
car_one.add_mileage(100)
car_one.add_mileage(-50)
car_one.final_mileage_reading()
car_one.current_mileage_reading(25099)
car_one.gas_tank_capacity()
car_one.default_tank_capacity_in_Ltr = 50
car_one.gas_tank_capacity()
print()

print("Electric Car instantiated")
my_tesla = ElectricCarClass("Tesla", "S model", "2010")
my_tesla.car_details()
my_tesla.battery.battery_capacity_in_kw()
my_tesla.battery.battery_size_in_qubic_ft()
my_tesla.gas_tank_capacity()
print("\nAfter changing default values of battery capacity and size:")
my_tesla.size_in_qft = 16
my_tesla.capacity_in_kw = 200
my_tesla.battery.battery_capacity_in_kw()
my_tesla.battery.battery_size_in_qubic_ft()

print("\nCalling methods from Parent class:")
my_tesla.current_mileage_reading(24000)
my_tesla.add_mileage(300)
my_tesla.final_mileage_reading()
