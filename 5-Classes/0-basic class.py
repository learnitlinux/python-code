class People:
    pass


ppl = People()
print(ppl)

# Next, we create an object/instance of this class using the name of the class followed by a
# pair of parentheses. For our verification, we confirm the type of the variable by simply printing it.
# It tells us that we have an instance of the Person class in the __main__ module-builtin.

# Notice that the address of the computer memory where your object is stored is also printed.
# The address will have a different value on your computer since Python can store the object
# wherever it finds space.
