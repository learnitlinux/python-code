class SchoolMember:
    def __init__(self, name, age):
        self.name = name
        self.age = age
        print(
            "A new member of the Little Flower school is added '{}'".format(self.name)
        )


# This program will accept multiple args in the Teacher subjects
class Teacher(SchoolMember):
    def __init__(self, *args, name, age, salary):
        SchoolMember.__init__(self, name, age)
        print(
            "Teacher name: {} \n "
            "Age: {}, \n "
            "Subjects: {}, \n "
            "Salary: {} \n".format(name, age, args, salary)
        )


class Student(SchoolMember):
    def __init__(self, name, age, grade, fee, percentage):
        SchoolMember.__init__(self, name, age)
        print(
            (
                "Student name {}, \n "
                "Age: {}, \n "
                "Grade: {}, \n "
                "Fee: {}$, \n "
                "Percentage: {:.2f} \n".format(name, age, grade, fee, percentage)
            )
        )


teach1 = Teacher("english", "maths", "science", name="madam1", age=30, salary=10000)
std1 = Student("std1", 15, "A1", 4000, 75.2)
