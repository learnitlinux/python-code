class NewEmployee:
    def __init__(self, first, last, salary):
        self.fname = first
        self.lname = last
        self.pay = salary
        self.email = first + "." + last + "@example.com"

    def fullname(self):
        return "{} {}".format(self.fname, self.lname)

    def pay_raise(self):
        self.pay = self.pay * 1.04


# In the below the __init__ method will be run and assign the variables.
new_emp1 = NewEmployee("Bharath", "Jakkani", 30000)
new_emp2 = NewEmployee("Nikhila", "Jakkani", 40000)

print(new_emp1.fname)
print(new_emp1.pay)

print()

print(new_emp2.fname)
print(new_emp2.pay)

print(
    """
Calling pay_raise method here for new_emp1, but this will only apply to the emp_1 but not for the other employees"""
)
new_emp1.pay_raise()
print(new_emp1.pay)
print("emp_2 pay is still not changed.", new_emp2.pay)
