class SchoolMember:
    def __init__(self, name, age):
        self.name = name
        self.age = age
        print(
            "A new member of the Little Flower school is added '{}'".format(self.name)
        )

    def info(self):
        print("Name: {} \n " "Age: {} ".format(self.name, self.age))


# This program will accept multiple args in the Teacher subjects
class Teacher(SchoolMember):
    def __init__(self, *args, name, age, salary):
        SchoolMember.__init__(self, name, age)
        self.salary = salary
        self.args = args

    def info(self):
        SchoolMember.info(self)
        print(" Subjects: {}, \n " "Salary: {} \n".format(self.args, self.salary))


class Student(SchoolMember):
    def __init__(self, name, age, grade, fee, percentage):
        SchoolMember.__init__(self, name, age)
        self.grade = grade
        self.fee = fee
        self.percentage = percentage

    def info(self):
        SchoolMember.info(self)
        print(
            (
                " Grade: {}, \n "
                "Fee: {}$, \n "
                "Percentage: {:.2f} \n".format(self.grade, self.fee, self.percentage)
            )
        )


teach1 = Teacher("english", "maths", "science", name="madam1", age=30, salary=10000)
teach1.info()
std1 = Student("std1", 15, "A1", 4000, 75.2)
std1.info()

print(help(Student))
