import datetime


class NewEmployee:
    raise_amount = 1.04  # class variable (4%raise)
    num_employees = 0

    def __init__(self, first, last, salary):
        # Instance variables are below which are started as self. and unique to each instance
        self.fname = first
        self.lname = last
        self.pay = salary
        self.email = first + "." + last + "@example.com"
        NewEmployee.num_employees += 1
        # As the __init__ method run each time an employee instance is created,
        # it make sense to increase the number by 1 each time.

    def fullname(self):
        return "{} {}".format(self.fname, self.lname)

    # def pay_raise_with_classname(self):
    #     self.pay = self.pay * NewEmployee.raise_amount
    #     # When you are using the class variables in methods, you must use the classname.variable

    def pay_raise_with_instance(self):
        self.pay = self.pay * self.raise_amount

    @classmethod
    def set_raise_amount(cls, amount):
        cls.raise_amount = amount

    @classmethod
    def string_to_values(cls, string):
        fname, lname, salary = string.split("-")
        return cls(fname, lname, salary)

    @staticmethod
    def week_day_or_not(given_date_in_string):
        if (
            datetime.date.weekday(given_date_in_string) == 5
            or datetime.date.weekday(given_date_in_string) == 6
        ):
            return False
        return True


new_emp1 = NewEmployee("Bharath", "Jakkani", 30000)
new_emp2 = NewEmployee("Nikhila", "Jakkani", 40000)

print(NewEmployee.raise_amount)
print(new_emp1.raise_amount)
print(new_emp2.raise_amount)

NewEmployee.set_raise_amount(1.05)
print("Class variable is changed to 5%")
print(NewEmployee.raise_amount)
print(new_emp1.raise_amount)
print(new_emp2.raise_amount)

# # This below is hardcode to get the first, last and email from a string
# # instead of from static method in a class
# emp_str3 = 'John-Smith-20000'
# first, last, salary = emp_str3.split("-")
# new_emp3 = NewEmployee(first, last, salary)
# print(new_emp3.fname)

# The below new employee strings are converted to values.

emp_str4 = "Raj-Singh-34000"
new_emp4 = NewEmployee.string_to_values(emp_str4)
print(new_emp4.fname, new_emp4.email)

given_date = datetime.date(2019, 11, 29)
print(given_date)

print(
    "given date is Weekday True or False:, {}"
    "\ngiven date numeric value: {}".format(
        NewEmployee.week_day_or_not(given_date), datetime.date.weekday(given_date)
    )
)
