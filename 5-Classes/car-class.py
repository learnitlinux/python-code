class CarModel:
    """
    Regular class model:
    The current class defines a car make, model and year of manufacture. If no initial
    mileage is set then current mileage is set to default to "0". If the car drives
    for extra miles (the value must be >0) then those will be added to current mileage
    after we call the function add_mileage. If user tries to tamper the current miles
    with less than what it was driven (current_mileage + add_mileage) then it gives
    an "Error" message.
    """

    def __init__(self, car_make, car_model, year_of_manufacture):
        self.make = car_make
        self.model = car_model
        self.year = year_of_manufacture
        self.mileage = 0

    def car_details(self):
        print(
            "\t Manufacturer: {}\n"
            "\t        Model: {}\n"
            "\t         Year: {}".format(self.make, self.model, self.year)
        )

    def current_mileage_reading(self, current_miles):
        if self.mileage > current_miles:
            print("Error: Cannot set a lower miles than already existing")
        else:
            self.mileage = current_miles
            print("  Current Mileage: {}".format(current_miles))

    def add_mileage(self, miles_added):
        if miles_added > 0:
            self.mileage += miles_added
            print("Added extra miles: {}".format(miles_added))
        else:
            print("Error: No negative values should be given")

    def final_mileage_reading(self):
        print("Final Mileage reading: {}".format(self.mileage))


print(CarModel.__doc__)
car_one = CarModel("Honda", "Civic", "2019")
car_one.car_details()
car_one.current_mileage_reading(25000)
car_one.add_mileage(100)
car_one.add_mileage(-50)
car_one.final_mileage_reading()
car_one.current_mileage_reading(25099)
