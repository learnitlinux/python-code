class Employee:
    def __init__(self, first_name, last_name, age, sex, id_no):
        self.first_name = str(first_name)
        self.last_name = str(last_name)
        self.age = int(age)
        self.sex = str(sex)
        self.id_no = int(id_no)

    def email_id(self):
        return "{}.{}@company.com".format(
            self.first_name.lower(), self.last_name.lower()
        )


class Developer(Employee):
    employee_count = 0

    def __init__(self, first_name, last_name, age, sex, id_no, *args):
        Employee.__init__(self, first_name, last_name, age, sex, id_no)
        self.skill_set = args
        Developer.employee_count += 1

    # return Developer.employee_count


class Manager(Employee):
    def __init__(self, first_name, last_name, age, sex, id_no, managing_team_name):
        Employee.__init__(self, first_name, last_name, age, sex, id_no)
        self.managing_team_name = managing_team_name

    @classmethod
    def reportees_of_manager(cls):
        for each_employee in range(Developer.employee_count):
            if each_employee is not None:
                xxxxxxxxxxxxxxx
            else:
                exit(123)


developer1 = Developer(
    "Bharath", "Jakkani", 34, "Male", 1234, ["java", "python", "linux"]
)
developer2 = Developer("Nikhila", "Jakkani", 30, "Female", 5678, ["java", "testing"])
developer3 = Developer(
    "Ravi",
    "Jakkani",
    29,
    "Male",
    9012,
    [
        "java",
        "python",
    ],
)
developer4 = Developer(
    "Susheel", "Jakkani", 27, "Male", 3456, ["java", "python", "c++", "linux"]
)
# print(developer1.email_info())
# print(developer1.skill_set)
# print(type(developer1.skill_set))
manager1 = Manager("John", "Smith", 40, "Male", 56789, "R&D Development")

# print(manager1.email_id())
print(Manager.reportees_of_manager())
