"""A set of classes used for Electric Car instances."""
from car import Car

# Created a Battery class separately from ElectricCarClass.


class Battery:
    def __init__(self):
        self.capacity_in_kw = 100
        self.size_in_qft = 12

    def battery_capacity_in_kw(self):
        print(" Battery Capacity: {}Kw".format(self.capacity_in_kw))

    def battery_size_in_qubic_ft(self):
        print("     Battery Size: {}qft".format(self.size_in_qft))


class ElectricCar(Car):
    """
    Method Override model class example:
    The current class defines a car make, model and year of manufacture. If no initial mileage
    is set then current mileage is set to default to "0". If the car drives for extra miles
    (the value must be >0) then those will be added to current mileage after we call the
    function add_mileage. If user tries to tamper the current miles with less than what it
    was driven (current_mileage + add_mileage) then it gives an "Error" message.

    Now, in this program we have implemented a subclass in the superclass called Electric Car.
    The methods in this subclass can only be called from subclass. We can override a method
    in the subclass driven from superclass, ex: gas_tank_capacity().

    We have created a new class Battery and assign that to an attribute of ElectricCar class.
    Now, we can call all the methods under Battery class using  dot notation.
    """

    def __init__(self, make, model, year):
        Car.__init__(self, make, model, year)
        # Here We added an attribute called self.battery, this line tells python to create a new
        # instance of a Battery and assign that instance to the attribute self.battery.
        self.battery = Battery()

    def gas_tank_capacity(self):
        print("Error: No Gas tank for Electric car")
