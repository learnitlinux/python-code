# import car
# import electric_car
from car import Car
from electric_car import ElectricCar as EC

# print(car.__doc__)
# print(electric_car.__doc__)

print(Car.__doc__)
car_one = Car("Honda", "Civic", "2019")
car_one.car_details()
car_one.current_mileage_reading(25000)
car_one.add_mileage(100)
car_one.add_mileage(-50)
car_one.final_mileage_reading()
car_one.current_mileage_reading(25099)
car_one.gas_tank_capacity()
car_one.default_tank_capacity_in_Ltr = 50
car_one.gas_tank_capacity()
print()

print("Electric Car instantiated")
print(EC.__doc__)
my_tesla = EC("Tesla", "S model", "2010")
my_tesla.car_details()
my_tesla.battery.battery_capacity_in_kw()
my_tesla.battery.battery_size_in_qubic_ft()
my_tesla.gas_tank_capacity()
print("\nAfter changing default values of battery capacity and size:")
my_tesla.size_in_qft = 16
my_tesla.capacity_in_kw = 200
my_tesla.battery.battery_capacity_in_kw()
my_tesla.battery.battery_size_in_qubic_ft()

print("\nCalling methods from Parent class:")
my_tesla.current_mileage_reading(24000)
my_tesla.add_mileage(300)
my_tesla.final_mileage_reading()
