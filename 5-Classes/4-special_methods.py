" Here we are explaining the usage of the __str__, __len__ and other special method from class"


class Book:
    def __init__(self, bookname, bookauthor, pages_in_book):
        self.name = bookname
        self.author = bookauthor
        self.pages = pages_in_book


my_book = Book("Python essentials", "Bharath", 100)
print(my_book)
try:
    len(my_book)
except TypeError as e:
    print(e)


class New_Book:
    def __init__(self, bookname, bookauthor, pages_in_book):
        self.name = bookname
        self.author = bookauthor
        self.pages = pages_in_book

    def __str__(self):
        return "{} by {} of pages {}".format(self.name, self.author, self.pages)

    def __len__(self):
        return self.pages

    def __del__(self):
        print("Book object has been deleted.")


print("\nLet's try again with defined special methods!")
new_book = New_Book("Python advanced", "Bharath", 300)
print(new_book)
print(len(new_book))
# del is to delet a variable defined
del new_book
