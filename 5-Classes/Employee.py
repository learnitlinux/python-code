class Employee:
    pass


# emp_1 and emp_2 are instance variables
emp_1 = Employee()
emp_2 = Employee()
print("==========The below shows the memory locations of the two instances ==========")
print(emp_1)
print(emp_2)
print("==========")

emp_1.first = "John"
emp_1.last = "Smith"
emp_1.email = "john.smith@example.com"
emp_1.salary = 10000

emp_2.first = "Mark"
emp_2.last = "Twain"
emp_2.email = "mark.twain@example.com"
emp_2.salary = 20000


print(emp_1.email)
print(emp_2.email)
print()

# Now we can do this with classes and reduce the coding and manual addition of employees


class NewEmployee:
    def __init__(self, first, last, salary):
        self.fname = first
        self.lname = last
        self.pay = salary
        self.email = first + "." + last + "@example.com"

    def fullname(self):
        return "{} {}".format(self.fname, self.lname)


# In the below the __init__ method will be run and assign the variables.
new_emp1 = NewEmployee("Bharath", "Jakkani", 30000)
new_emp2 = NewEmployee("Nikhila", "Jakkani", 40000)

print(new_emp1.fname)
print(new_emp1.fullname())
print(new_emp1.pay)
print()
print(new_emp2.fname)
print(new_emp2.fullname())
print(new_emp2.pay)

print()

# this is another way of calling a method with class.method(instance_name), this is how the instance will be passed to
# the method(here fullname) of the class.
print(NewEmployee.fullname(new_emp1))
