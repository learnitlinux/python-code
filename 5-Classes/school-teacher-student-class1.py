class SchoolMember:
    def __init__(self, name, age):
        self.name = name
        self.age = age
        print(
            "A new member of the Little Flower school is added '{}'".format(self.name)
        )


class Teacher(SchoolMember):
    def __init__(self, name, age, subjects, salary):
        SchoolMember.__init__(self, name, age)
        print(
            "Teacher: name {}\n "
            "Age: {}, \n "
            "Subjects: {}, \n "
            "Salary:".format(name, age, subjects, salary)
        )


class Student(SchoolMember):
    def __init__(self, name, age, grade, fee, percentage):
        SchoolMember.__init__(self, name, age)
        print(
            (
                "Student Name: {}, \n "
                "Age: {}, \n "
                "Grade: {}, \n "
                "Fee: {}, \n "
                "Percentage: {}".format(name, age, grade, fee, percentage)
            )
        )


teach1 = Teacher("madam1", 30, "english", 10000)
std1 = Student("std1", 15, "A1", "4000$", "75%")


# Using the isinstance() and issubclass() functions here, these functions help to validate whether an object is
# instance of a class
print("===================")
print("Is teach1 object is instance of Teacher class: ", isinstance(teach1, Teacher))
print(
    "Is teach1 object is instance of SchoolMember class: ",
    isinstance(teach1, SchoolMember),
)
print("Is std1 object is instance of Teacher class: ", isinstance(std1, Teacher))
print(
    "Is Student class is subclass of SchoolMember class: ",
    issubclass(Student, SchoolMember),
)
print(
    "Is Teacher class is subclass of SchoolMember class: ",
    issubclass(Teacher, SchoolMember),
)
print("Is Student class is subclass of Teacher class: ", issubclass(Student, Teacher))


# Enable this to see the how a class is inherited from other classes in help function.
print(help(Student))
