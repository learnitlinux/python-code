class Persons:
    def __init__(self):
        print("init")

    def hello(self):
        print("hello")


# The __init__ method is run as soon as an object of a class is instantiated, like below. 'per' is a instance of the
# class named persons. If you create 'N' number of such instances then the __init__ will run 'N' times.

print(
    "When you instantiate a class, the object will be filled in 'self', don't forget! the brackets () are a must."
)
per = Persons()  # init word will be printed to show that it is instantiated
per1 = Persons()  # init word will be printed to show that it is instantiated
per1.hello()


class PersonCount:
    num = 0  # this is class variable

    def __init__(
        self,
    ):
        print("hi new employee")
        PersonCount.num += 1


emp1 = PersonCount()
emp2 = PersonCount()
emp3 = PersonCount()
print("You have initiated instances of the class for {} times".format(PersonCount.num))
