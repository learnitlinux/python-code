import time

poem = """Hello world
John Cena
Jim Carrey
Sheldon Cooper
"""
f = open("file_name.txt", "w")
f.write(poem)
f.close()

f = open("file_name.txt", "r")
try:
    while True:
        string_name = f.readline()

        if len(string_name) == 0:
            break
        else:
            print(string_name)
            time.sleep(2)


except KeyboardInterrupt:
    print("You have entered ctrl + c")
except EOFError:
    print("You have abruptly ended the process")
# except FileNotFoundError:
#    print("there is no such file.")
except IOError:
    print("file not found error")

finally:
    if f:
        f.close()
    print("closed the file")
