#!/usr/bin/python3.6


class ShortNameException(Exception):
    def __init__(self, length):
        self.length = length


print(
    "\nTry entering username of length, less than 8 characters and see what happens :)"
)
while True:
    try:
        text = input("Please enter your username: ")
        if len(text) < 8:
            raise ShortNameException(len(text))

    except ShortNameException:
        print("You have entered less than 8 characters")
    except KeyboardInterrupt:
        print("You have entered ctrl + c")
    except EOFError:
        print("You have entered ctrl + d")

    else:
        print("You have entered the text {} of length {}".format(text, len(text)))
        continue
