while True:
    try:
        num1 = input("Please enter first number/ 'q' to quit: ")
        if num1 == "q":
            break
        num1 = int(num1)
        num2 = input("Please enter second number/ 'q' to quit: ")
        if num2 == "q":
            break
        num2 = int(num2)

    except ValueError:
        print("Please enter a number not string or characters")
    else:
        print("Total is: {}".format(num1 + num2))
