#!/usr/bin/python3.6

print("\nBelow you can enter username or try ctr+c, ctrl+d")
while True:
    try:
        username = input("Please Enter username: ")
        print(
            "You have entered a username: {}, of length {}".format(
                username, len(username)
            )
        )
        if len(username) is not 0:
            break
    # This exception is very broad, as you can see there is nothing specified, if you don't specify
    # any exceptions, it will handle all errors and exceptions.
    except:
        print("you did not enter username, please check")
