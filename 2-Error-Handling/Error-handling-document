There are (at least) two distinguishable kinds of errors: syntax errors and exceptions.
Even if a statement or expression is syntactically correct, it may cause an error when an attempt is made to execute it.
Errors detected during execution are called exceptions.

- The string printed as the exception type is the name of the built-in exception that occurred.
Ex: ZeroDivisionError, NameError and TypeError

The try statement works as follows.

First, the try clause (the statement(s) between the try and except keywords) is executed.

If no exception occurs, the except clause is skipped and execution of the try statement is finished.

If an exception occurs during execution of the try clause, the rest of the clause is skipped. Then if its type matches
the exception named after the except keyword, the except clause is executed, and then execution continues after the try statement.

If an exception occurs which does not match the exception named in the except clause, it is passed on to outer try statements;
if no handler is found, it is an unhandled exception and execution stops with a message as shown above.

an except clause may name multiple exceptions as a parenthesized tuple, for example:

... except (RuntimeError, TypeError, NameError):
...     pass

A class in an except clause is compatible with an exception if it is the same class or a base class thereof (but not the other way around ? an except clause listing a derived class is not compatible with a base class). For example, the following code will print B, C, D in that order:

class B(Exception):
    pass

class C(B):
    pass

class D(C):
    pass

for cls in [B, C, D]:
    try:
        raise cls()
    except D:
        print("D")
    except C:
        print("C")
    except B:
        print("B")
Note that if the except clauses were reversed (with except B first), it would have printed B, B, B ? the first matching
except clause is triggered.

The try ? except statement has an optional else clause, which, when present, must follow all except clauses.
It is useful for code that must be executed if the try clause does not raise an exception.




