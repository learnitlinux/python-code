a = input(
    "Enter a number: ",
)
b = input("Enter another number: ")

try:
    print(int(a) / int(b))

except Exception as ae:
    print(ae)

# The Exception as xx and printing the variable xx will show what kind of error is occurred. Now, we can see error and
# modify the code according to the new error.

# Output:
# Enter a number: >? 10
# Enter another number: >? a
# invalid literal for int() with base 10: 'a'

# Enter a number: >? 10
# Enter another number: >? 0
# division by zero

# Please uncomment above exception and run with same as above and you get these exception messages below.
except ZeroDivisionError:
    print("You have given a zero as a second integer, No number is divisible by zero.")

except ValueError:
    print("You did not give the integers for the division.")

# you can also combine the exceptions, to see this message, comment all the exceptions above.
except (ZeroDivisionError, ValueError):
    print("Please provide the correct values.")
