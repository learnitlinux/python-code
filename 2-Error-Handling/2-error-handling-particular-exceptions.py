#!/usr/bin/python3.6
print(
    "\nYou can enter a text or press ctrl+c or ctrl+d to see how the exceptions are handled."
)
while True:
    try:
        text = input("Enter something here: ")

    except KeyboardInterrupt:
        print("You have pressed ctrl + c")

    except EOFError:
        print("You have pressed ctrl + d")

    # This below code handles all exceptions.
    # except:
    #     print("\nYou have not entered a valid text\n")
    #     continue
    else:
        print("you have entered the text: {}".format(text))
        break
