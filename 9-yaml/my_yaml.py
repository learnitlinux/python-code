import yaml
from yaml import Loader

yaml_file = open("company.yaml", "r")
# yaml.load needs Loader so we imported twice above.
# not sure, if there is a better way to do it yet!
data = yaml.load(yaml_file, Loader=Loader)
print("HQ Results:", data["HQ"])
print("HR Results:", data["HR"])
print("HR-Inernal Results:", data["HR"]["Internal"])
print("Finance Results:", data["Finance"])
print("IT Results:", data["IT"])
print("IT-Security Results:", data["IT"][0]["Security"])
print("IT-Help Desk Results:", data["IT"][1])
print("HR-Internal Results:", data["HR"]["Internal"])
print("Container-Results:", data["metadata"])
print("multiline-Results:", data["multiline"])
print("single line-Results:", data["singleline"])
