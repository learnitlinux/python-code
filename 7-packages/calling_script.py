from MainPackage import main_script
from MainPackage.SubPackage import sub_script

main_script.my_instance.func_in_mainscript()
sub_script.my_sub_instance.func_in_subscript()

another_main_instance = main_script.MymainModule("by Sunil")
another_main_instance.func_in_mainscript()

another_sub_instance = sub_script.MysubModule("by Sunil")
another_sub_instance.func_in_subscript()
