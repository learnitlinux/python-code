print("Sets datatype tutorial:")
print(
    "A set is an unordered collection with no duplicate elements. Basic uses include membership testing and "
    "eliminating duplicate entries"
)
print()

print("Step 1")
my_fruits_set = {"orange", "orange", "banana", "apple", "apple"}
print("my_fruit_set:", my_fruits_set)
print("'my_fruits_set' Class type:", type(my_fruits_set))

my_groceries_set = {"onions", "banana", "onions", "lettuce", "milk"}
print("my_groceries_set:", my_groceries_set)
print("Intersection of both sets:", my_fruits_set.intersection(my_groceries_set))
print("Union of both sets:", my_fruits_set.union(my_groceries_set))
print()

print("Step 2")
print("'Symmetric_difference will exclude the common elements'")
# Returns a new set.
print(my_fruits_set ^ my_groceries_set)
# OR
# print(my_fruits_set.symmetric_difference(my_groceries_set))

print("\nsymmetric_difference_update will update the original set")
print("my_fruits_set before:", my_fruits_set)
my_fruits_set.symmetric_difference_update(my_groceries_set)
print("my_fruits_set after:", my_fruits_set)
print()

print("Step 3")
# Resetting back the my_fruits_set to original.
my_fruits_set = {"orange", "orange", "banana", "apple", "apple"}
print(
    "Difference of my_fruits_set to my_groceries_set: ",
    my_fruits_set.difference(my_groceries_set),
)
print("Difference and update will update the original set")
my_fruits_set.difference_update(my_groceries_set)
print("my_fruit_set:", my_fruits_set)

# Resetting back the my_fruits_set to its original.
my_fruits_set = {"orange", "orange", "banana", "apple", "apple"}
print()

print("Step 4")
# Working with multiple sets.
s1 = {2, 2, 3, 4, 5}
s2 = {2, 3, 6, 6, 7}
s3 = {"apple", "banana", "banana", 2, 5}
print("s1, s2, s3 intersection:", set.intersection(s1, s2, s3))
print("s1, s2, s3 difference:", set.difference(s1, s2, s3))
print("s1, s2, s3 union:", set.union(s1, s2, s3))
print()

print("Step 5")
s4 = set.copy(s1)
print("Copy of set s1 - s4 is:", s4)
s4.clear()
print("Set s4 is cleared set:", s4)
s1.add(34)
print("Add method on s1:", s1)
s2.remove(2)
print("Remove method on s2:", s2)
s2.discard(3)
print("Discard method on s2:", s2)
s2.pop()
# Remove the first item.
print("Pop method on s2:", s2)
s2.update([11, 22, 22, 33])
s2.update({"name": "john", "age": 23})
# Update the keys only
print("S2 Now:", s2)
print()

# frozen sets
print("frozen set is an immutable set. They are also hashable")
# hashable An object is hashable if it has a hash value which never changes during its lifetime (it needs a __hash__(
# ) method), and can be compared to other objects (it needs an __eq__() method). Hashable objects which compare equal
# must have the same hash value. Hashability makes an object usable as a dictionary key and a set member,
# because these data structures use the hash value internally.
# All of Python's immutable built-in objects are hashable; mutable containers (such as lists or dictionaries) are not.

print(
    "frozen sets have the same functions as normal sets, except none of the functions that change the contents"
    "(update, remove, pop, etc.) are available."
)

my_list = ["John", "Jack", "Andy", 1000, 2000]
fr_set = frozenset(my_list)
print(fr_set)
