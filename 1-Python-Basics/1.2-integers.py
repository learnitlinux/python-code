print("Step1:")
a = 10
b = 20
print("concatenation of a and b:", a + b)
# Here is the sample of how above addition works, but "__add__" is little advance topic and can be ignored here.
print("concatenation of a and b using __add__ method:", int.__add__(a, b))

string_1 = "Hello"
string_2 = " John"
# Here is the sample of how two strings are added, using string object "__add__" method. but "__add__" is little
# advance topic and can be ignored here.
print("concatenation of string1 and string2:", string_1 + string_2)
print(
    "concatenation of string1 and string2 using __add_method:",
    str.__add__(string_1, string_2),
)
print()

print("Step2:")
print(
    """
Types of numbers:

int (signed integers) ? They are often called just integers or ints. They are positive or negative whole numbers with 
no decimal point. Integers in Python 3 are of unlimited size. Python 2 has two integer types - int and long. There is 
no 'long integer' in Python 3 anymore. 

float (floating point real values) ? Also called floats, they represent real numbers and are written with a decimal 
point dividing the integer and the fractional parts. Floats may also be in scientific notation, with E or e 
indicating the power of 10 (2.5e2 = 2.5 x 100 = 250).

Here is a table of the two main types with some examples:
1,2,-5,1000	Integers
1.2,-0.5,2e2,3E2	Floating-point numbers

complex (complex numbers) ? are of the form a + bJ, where a and b are floats and J (or j) represents the square root 
of -1 (which is an imaginary number). The real part of the number is a, and the imaginary part is b. Complex numbers 
are not used much in Python programming. 
Here are some examples of numbers.

int	        float	    complex
10	        0.0	        3.14j
100	        15.20	    45.j
-786	    -21.9	    9.322e-36j
080	        32.3+e18	.876j
-0490	    -90.	    -.6545+0J
-0×260	    -32.54e100	3e+26J
0×69	    70.2-E12	4.53e-7j

"""
)
# Arithmetic operations

print("Addition of 10 and 2:", 10 + 2)
print("Subtraction of 10 and 2:", 10 - 2)
print("Multiplication of 10 and 2:", 10 * 2)
print('Exponential, 10 to the power of two:', 10 ** 2)
print("Division of 10 by 2:", 10 / 2)
print("Division of 10 by 2.0 float:", 10 / 2.0)
print("Modulus, remainder of 10 by 3:", 10 % 3)
# Dividend: The number that is being divided.
# Divisor: The number by which the dividend is divided.
# Quotient: The result of the division.
# Remainder: What is left over after the division (if the division is not exact).
# For example, in the division
# 20 ÷ 4 = 5
# 20 is the dividend.
# 4 is the divisor.
# 5 is the quotient.

print(
    """\nFloor Division - The division of operands where the result is the quotient in which the digits after the 
decimal point are removed. But if one of the operands is negative, the result is floored, i.e., rounded away from 
zero ( towards negative infinity)\n"""
)
print("Floor division/quotient, of 10 and 6:", 10 // 6)
print("Floor division/quotient, of -10 and 6:", -10 // 6)
print()

print("Step 3:")
print(
    """The names you use when creating these labels need to follow a few rules:

1. Names can not start with a number.
2. There can be no spaces in the name, use _ instead.
3. Can't use any of these symbols :'",<>/?|\()!@#$%^&*~-+
4. It's considered best practice (PEP8) that names are lowercase.
5. Avoid using the characters 'l' (lowercase letter el), 'O' (uppercase letter oh), 
   or 'I' (uppercase letter eye) as single character variable names.
6. Avoid using words that have special meaning in Python like "list" and "str"
"""
)

print(
    """Type int(x) to convert x to a plain integer. Type long(x) to convert x to a long integer. Type float(x) to 
    convert x to a floating-point number. Type complex(x) to convert x to a complex number with real part x and 
    imaginary part zero. Type complex(x, y) to convert x and y to a complex number with real part x and imaginary 
    part y. x and y are numeric expressions"""
)

print("int(2.34): ", int(2.34))
print("float(23): ", float(23))
print("complex(2):", complex(2))
print("complex(2, 3)", complex(2, 3))

print(
    """\nDynamic Typing Python uses dynamic typing, meaning you can reassign variables to different data types. This 
makes Python very flexible in assigning data types; it differs from other languages that are statically typed."""
)

my_dogs = 2
print("my_dogs: ", my_dogs)
my_dogs = ["Sally", "Robo"]
print("my_dogs: ", my_dogs)
exit(0)
