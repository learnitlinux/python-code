print("Boolean document\n", bool.__doc__)
print(
    """
==	If the values of two operands are equal, then the condition becomes true.
!=	If values of two operands are not equal, then condition becomes true.
>	If the value of left operand is greater than the value of right operand, then condition becomes true.
<	If the value of left operand is less than the value of right operand, then condition becomes true.
>=	If the value of left operand is greater than or equal to the value of right operand, then condition becomes true.
<=	If the value of left operand is less than or equal to the value of right operand, then condition becomes true.
"""
)

print("Step 1:")
print(type(False), "'OR'", type(None), "'OR'", type(True), "OR", type(not False))
print("1 > 2:", 1 > 2)
print("1 == 1.0:", 1 == 1.0)
print()

# Chaining comparisons operators in Python 'and', 'or', 'not'.
print("1 < 2 > 3:", 1 < 2 > 3)
print("1 < 2 and 2 > 3:", 1 < 2 and 2 > 3)
print("1 < 2 or 2 > 3:", 1 < 2 or 2 > 3)
print("not 1 == 1:", not 1 == 1)
