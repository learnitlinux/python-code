"""
LEGB Rule:
L: Local ? Names assigned in any way within a function (def or lambda), and not declared global in that function.
E: Enclosing function locals ? Names in the local scope of any and all enclosing functions (def or lambda), from inner to outer.
G: Global (module) ? Names assigned at the top-level of a module file, or declared global in a def within the file.
B: Built-in (Python) ? Names preassigned in the built-in names module : open, range, SyntaxError,..
"""
x = 25


def print_x():
    x = 30
    return x


print("Step 1:")
print("x value is:", x)
print("Because the scope of x=30 is still in the scope of print_x function")

print_x()
print("x value still not changed: ", x)
print(
    "'As print_x is returning a value of x but not printing it, so we must print the function call itself to return "
    "\nlocal x value'"
)
print("x value now is:", print_x())
print()


print("Step 2:")


def print_y():
    # Not recommended for the beginners to use the global keyword as it affects the value globally.
    global y
    y = 100
    print(
        "print_y() is called, and y is declared as global variable inside the function, "
        "y value inside the function is:",
        y,
    )
    return y


y = 50
print("y value is: ", y)

print_y()
print("y value after calling the function print_y and affecting it globally:", y)
print()

print("Step 3:")
print(
    """  
    Python follows LEGB RULE: 
    Local(L): Defined inside function/class
    Enclosed(E): Defined inside enclosing functions(Nested function concept)
    Global(G): Defined at the uppermost level
    Built-in(B): Reserved names in Python builtin modules"""
)
print()
# Global(G)
name = "John"


def name_change():
    # Enclosed function(E)
    name = "Wick"
    print("'name' inside Enclosed: ", name)

    def hello_name():
        # Local(L)
        name = "John Wick"
        print("Hello: " + name)

    hello_name()
    return "name in function return: " + name


print(name_change())
print("'name' in global: ", name)
print(
    "Try commenting the 'name' variable in every level - L, E and see how it changes!"
)
print(
    """
    It is not a good idea to declare any variable as 'Global', because it will be hard to troubleshoot if multiple
    scripts inside a project using the same variable but you declare it as 'Global' inside another method without
    knowing it!.
    Instead of that we can assign the function to the 'same variable' and use it...\n"""
)

print("A sample example is below...\n")
my_var = "Global"
print("my_var is:", my_var)


def local_var():
    my_var = "Local"
    return my_var


my_var = local_var()
print("my_var is:", my_var)
