var1 = 10
print("var1= ", var1)
# Here we are un setting the value of variable 'var1'
del var1

try:
    print("var1= ", var1)

except NameError as my_error:
    print(my_error)

my_list = [10, 50.6, 78, 45, 30.9, 1920]
del my_list[1:5]
print(my_list)
