# Arithmetic Operators:
# Addition:       3 + 2
# Subtraction:    3 - 2
# Multiplication: 3 * 2
# Division:       3 / 2
# Floor Division: 3 // 2
# Exponent:       3 ** 2
# Modulus:        3 % 2


# Comparisons:
# Equal:            3 == 2
# Not Equal:        3 != 2
# Greater Than:     3 > 2
# Less Than:        3 < 2
# Greater or Equal: 3 >= 2
# Less or Equal:    3 <= 2


global_num = 10
decimal = 3.14
print(type(global_num), end="\n\n")
print(type(decimal), end="\n\n")
print(3 * (2 + 5))  # 21

global_num += 1
print(global_num)  # 11
global_num *= 10  # works with +/-/*/ / /**
print(global_num)  # 110
global_num //= 11
print(global_num)  # 10
global_num /= 3
print(global_num)  # 3.3333

print(abs(-3))  # 3
print(round(3.4))  # 3
print(round(3.5))  # 4
print(round(3.454, 2))  # 3.45
print(round(3.455, 2))  # 3.46
