from random import randint

number_of_guesses = 0
magic_number = randint(1, 100)
# print("Magic Number is: ", magic_number)

while True:
    your_guess_number = int(input("Enter number between 1 to 100 here: "))
    print("your_guess_number: ", your_guess_number)
    if your_guess_number not in range(1, 101):
        print("OUT OF BOUNDS")
        break

    number_of_guesses += 1
    if magic_number == your_guess_number:
        print("You Guessed it correct!")
        print("Number of guesses took:", number_of_guesses, "guess(es)")
        break

    if number_of_guesses == 1:
        if abs(magic_number - your_guess_number) <= 10:
            print("WARM!")
        else:
            print("COLD!")

    else:
        diff_a = abs(store_your_previous_number - magic_number)
        diff_b = abs(your_guess_number - magic_number)
        if diff_a >= diff_b:
            print("WARMER!")
        else:
            print("COLDER!")
    store_your_previous_number = your_guess_number
