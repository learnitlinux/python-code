# There are three ways to perform string formatting.
#
# The oldest method involves placeholders using the modulo % character.
# An improved technique uses the .format() string method.
# The newest method, introduced with Python 3.6, uses formatted string literals, called f-strings.
# Since you will likely encounter all three versions in someone else's code.

print("Step 1:")

# >>>>>>>>>>> format with "place holders" <<<<<<<<<<<<
print(" >>>>>>>>>>> format with 'place holders' <<<<<<<<<<<< ")

print("print something here: %s, and more here: %s" % ("Hello World", "End"))
print(
    "The difference between %s and %r because of quotes"
    % ("there is \ttab", "there is no \ttab")
)

x, y = 10, 20.3456
# The %s operator converts whatever it sees into a string, including integers and floats.
# The %d operator converts numbers to integers first, without rounding. Note the difference below:
print("The value of X is %s, and Y is %s" % (x, y))
print("The value of X is %d and Y is %d" % (x, y))
print()

# >>>>>>>>>>>>> 'format' method examples <<<<<<<<<<<<
print(" >>>>>>>>>>>>> 'format' method examples <<<<<<<<<<<< ")

print("The {} {} {}".format("bang", "big", "theory"))
print("The {1} {0} {2}".format("bang", "big", "theory"))
print("The {first} {second} {third}".format(second="bang", first="big", third="theory"))

print("\npadding in format with {:xxxx}")
for i in range(1, 5):
    print("Number is {:03}".format(i))
print("\n")

pi = 3.14567890

print("pi value upto 2 decimal points is {:.2f}".format(pi))
print("pi value upto 5 decimal points is {:.5f}".format(pi))

# Alignment, padding and precision wit .format()
# float format follows - {value:width.precisionf)
print(">{0:10.4f}<".format(22 / 7))
print("length>", len("{0:8.4f}".format(22 / 7)))

# I observed that if the width value is less than the actual length is displayed it does not affect any, but if it is
# more then it will add the extra space, try removing value 11 and change it from 1 to 9 it did not change any thing in
# output.
print("{0:11.3f}".format(10234.12345))

print("{0:10}|{1:6}{2}".format("Fruits", "Price", "$"))
print("{0:10}|{1:5}".format("Oranges", 3.35))
print("{0:10}|{1:5}".format("Apples", 4.35))
print("{0:10}|{1:5}".format("Kiwi", 6.35))


print(
    """
By default, .format() aligns text to the left, numbers to the right.
You can pass an optional <,^, or > to set a left, center or right alignment:
you can use the padding characther like - . *\n"""
)

print("{0:*^14} | {1:*^12}".format("Fruit", "Quantity"))
print("{0:_^14} | {1:-<9}".format("Apples", 3.99))
print("{0:_^14} | {1:.<9}".format("Oranges", 10.56))
print()

print("Step 2:")
print(
    """
Introduced in Python 3.6, f-strings offer several benefits over the older .format() string method described 
above. For one, you can bring outside variables immediately into to the string rather than pass them as arguments 
through .format(var).
    """
)

name = "Sam"
age = 30
# !r for the string representation.
print(f"My name is {name!r}. I am {age} years old.")

# f strings needs the precision means how many total number of digits you want to display. Unfortunately f-strings
# do not pad to the right of the decimal, even if the precision allows it, see below.
num = 23.45
print("My 10 character, four decimal number is:{0:10.4f}".format(num))
print(f"My 10 character, but not a four decimal number is:{num:{10}.{6}}")

# If this becomes important, you can always use .format() method syntax inside an f-string:

print(f"My 10 character, four decimal number is: {num:10.4f}")
