def print_times(message, times=5):
    print(message * times)


# def another_one(times=3, message):
# SyntaxError: non-default argument follows default argument
# print(message * times)

# another_one('world')


def no_order_args(d, b, a=10, c=30):
    # but here the default arguments should be last and non default should be frist
    print("sum is {}".format(a + b + c + d))


def variable_args(*mylist):
    for i in mylist:
        print("i value is: {}".format(i))


def variable_dict(**dict):
    for key, value in dict.items():
        print(key, value)


def return_value(a, b):

    """The current method returns the bigger number.

    Please give the numberss alone but not strings."""

    if a > b:
        return "a is a bigger number"
    elif a == b:
        return "a and b are equal"
    else:
        return "b is bigger number"


def none_function():
    pass


print_times("hello ")
print("\n\n")

print_times("world ", times=3)
print("\n\n")

no_order_args(40, 20)
print("\n\n")

variable_args(10, 20, 30, "hello")
print("\n\n")

variable_dict(name="bharath", age=30, city="Hyderabad")
print("\n\n")

print(return_value(b=20, a=30))
print(return_value.__doc__)
print("\n\n")

print(none_function())
