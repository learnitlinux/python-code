print("Literals")
print("A literal is data whose values are determined by the literal itself.")
print(123)
print(
    """
Take a look at the following set of digits: 123
Can you guess what value it represents? Of course you can ? it?s one hundred twenty three. But what about this: c

Does it represent any value? Maybe. It can be the symbol of the speed of light, for example. It also can be the 
constant of integration. Or even the length of a hypotenuse in the sense of a Pythagorean theorem. There are many 
possibilities. You cannot choose the right one without some additional knowledge. 
And this is the clue ? 123 is a literal, and c is not."""
)
