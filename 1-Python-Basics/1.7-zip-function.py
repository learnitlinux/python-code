print(
    """
You can use the zip() function to quickly create a list of tuples by "zipping" up together two lists.
"""
)

list1 = ["Name", "Hobby", "Fav-dish"]
list2 = ["Bob", "painting", "Chicken Tikka Masala"]

print("Prints only memory location: ", zip(list1, list2))

print("Converting to dictionary: ", dict(zip(list1, list2)))
print("Converting to tuple: ", tuple(zip(list1, list2)))
print("Converting to list: ", list(zip(list1, list2)))
print()


for key, value in zip(list1, list2):
    print("Key: ", key)
    print("Value: ", value)
    print()
