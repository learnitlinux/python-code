# while True:
#     num = int(input('Enter the magic number: '))
#     if num == 10:
#         print ('You found the magic number, 10')
#         break
#     else:
#         print('Continue')
print("Step 1:")
numbers = [1, 2, 3, 4, 5, 6]

for num in numbers:
    if num == 3:
        print(num, ",is found!")
        # break
        continue
    print(num)

chars = "abcd"

# loop in loop
for num in numbers:
    for char in chars:
        print((num, char), end=";")
print()
for i in range(1, 10):
    print(i)

# When you don't want to use the variable name, you can use the simple "_"
for _ in "Hello":
    print("cool!")
print()

print("Step 2:")
# Tuple unpacking.
list_of_tuples = [(1, 2, 3), (4, 5, 6), (7, 8, 9)]

# for a,b,c in list_of_tuples:
# OR
for (a, b, c) in list_of_tuples:
    print(a)
    print(b)
    print(c)
# Now, going a little complicate. Creating a variable of tuple to unpack the tuples.
# This will help in unpacking long tuples of size more than 10...for example, so we
# don't have to put (a, b, c, .....) in the for loop.

sample_list = []
for item in range(1, 4):
    sample_list.append("item{}".format(item))

sample_tuple = tuple(sample_list)

print(sample_tuple)
list_of_tuples = [(1, 2, 3), (4, 5, 6), (7, 8, 9)]

# Now, we can use the 'sample_tuple' variable to unpack the tuple.
print("unpacking second time:")
for sample_tuple in list_of_tuples:
    print(sample_tuple[0])
    print(sample_tuple[1])
    print(sample_tuple[2])

print()

print("Step 4:")
# Like above we can unpack the dictionary too.

sample_dictionary = {
    "apple": 2.99,
    "orange": 3.99,
    "kiwi": 4.99,
    "banana": 1.99,
    "blueberry": 4.99,
    "raspberry": 3.99,
}
print(
    "As dictionaries are un ordered, if the dictionary is large, they may not print in the same order as they exist."
)
for key, value in sample_dictionary.items():
    print("fruit_name: {0:10} price: {1:2}".format(key, value))
print()
# break is to break out of the current closest enclosing loop.
# continue is to go to the top of the closest enclosing loop.
# pass is to do nothing at all.

for item in (1, 2, 3):
    if item == 2:
        continue
    print("Item: ", item)
for item in [1, 2, 3]:
    pass
print("End of pass")
