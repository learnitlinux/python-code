print(
    """
Arbitrary arguments:
Sometimes, we do not know in advance the number of arguments that will be passed into a function.
Python allows us to handle this kind of situation through function calls with an arbitrary number of arguments.

args meant the positional arguments, kwargs are key=value arguments, to expand args and kwargs we can use 
any word with * and ** respectively, for example *new_arg, **new_kwarg, or *spam, **kw_spam but we keep these as it is 
for easy understanding.\n"""
)

print("Step1: ")
# list as an example -- format string
first_list = ["John", 23]

print("My name is {}, I am {} old".format(first_list[0], first_list[1]))
print("######### unpacking the list ##############")
print("My name is {0}, I am {1} old".format(*first_list), end="\n\n")


def args_sum_func(*args):
    print("Positional arguments are expanded as tuple: ", args)
    return sum(args)


# We can give any number of integers in below function calling.
return_value = args_sum_func(10, 20, 30)
print("Sum of given an Arbitrary numbers:", return_value)
print()

my_list = []
my_list.extend(range(1, 11))
# print(my_list)
# Here we must expand the list as *my_list, else the function expecting integers and you are supplying a list, which
# Won't match!
return_value = args_sum_func(*my_list)
print("Total sum of 1 to 10 integers:", return_value)
print()


def kwargs_function(**kwargs):
    print("kwargs return a dictionary: ", kwargs)
    if "name" in kwargs:
        print("Name is: {}".format(kwargs["name"]))


kwargs_function(name="John", city="New York")


def args_kwargs_function(*args, **kwargs):
    # positional args and then followed by kwargs
    print("Sum of all the positional args:", sum(args))
    print("Keyword args are:", kwargs)
    print(
        "Pick few args, kwargs: arg[0]={}, kwargs['name']={}".format(
            args[0], kwargs["name"]
        )
    )


# Placing keyworded arguments ahead of positional arguments raises an exception:
# args_kwargs_function(name="Mathews", subject="Maths", school="St. Mary's High School", 10, 20, 30) # try un comment
args_kwargs_function(
    10, 20, 30, name="Mathews", subject="Maths", school="St. Mary's High School"
)
print()

print("Step2:")


def student_inf(*args, **kwargs):
    print(args)
    print(kwargs)


student_inf("Maths", "Physics", name="john", age=35, salary=12345)

info = {"Name": "John", "Age": 30, "Salary": 12345}
skills = ["java", "python", "git"]

print()
print("Step3:")


def emp_info(*args, **kwargs):
    print(args)
    print(kwargs)


# emp_info(skills, info)  - not a good idea to use it, unpack them before use
emp_info(*skills, **info)


list1 = []


def my_func(*args):
    for item in args:
        if item % 2 == 0:
            list1.append(item)
    return list1


even_list = my_func(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
print("Even list: {}".format(even_list))
