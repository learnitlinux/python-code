# Tutorial
# https://github.com/Pierian-Data/Complete-Python-3-Bootcamp/blob/master/01-Python%20Comparison%20Operators/01-Comparison%20Operators.ipynb

print("1.", 2 == 2)
print("2.", "2" != 2)
print("3.", 2.0 == 2)
print("4.", "Bye" != "bye")
print("5.", 3 <= 4)
print("6.", 6 >= 5)

# chaining comparison operators
print("7.", 1 < 2 < 3)
print("8.", 1 < 2 and 4 > 3)
print("9.", "Bye" == "bye" or 1 < 2)
print("10", not 1 == 2)
