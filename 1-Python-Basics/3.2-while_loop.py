print("Step 1:")
x = 0
while True:
    print("Hello World!")
    x += 1
    if x == 3:
        break
# break is to break out of the current closest enclosing loop.
# continue is to go to the top of the closest enclosing loop.
# pass is to do nothing at all.
