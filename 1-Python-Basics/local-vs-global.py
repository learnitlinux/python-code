global_num = 10
local_num = 3.14


def global_var():  ### we cannot do both - passing the variable as arguement and declare it as global.
    global global_num
    print(
        "value of global_num is {} which assigned in main function".format(global_num)
    )
    global_num = 20
    print(
        "now the value of global_num is changed to {} in the function".format(
            global_num
        )
    )


def local_var(
    local_num,
):  # we must pass the local variables as parameters in to the function, else they error out.
    print(
        "value of the local_num is still {} which assigned in main function".format(
            local_num
        )
    )
    local_num = 2.0
    print(
        "now the value of local_num is changed to {} in local function".format(
            local_num
        )
    )


global_var()
print(
    "the global_num value is still {} when printed in main function".format(global_num),
    end="\n\n",
)

local_var(local_num)
print("The local_num value is back to {}".format(local_num))
