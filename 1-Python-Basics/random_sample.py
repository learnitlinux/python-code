import random

# gives a float between 0 and 1 but not include both of them
value = random.random()
print(value)

# to get random float value between two a certail range...
value2 = random.uniform(20, 30)
print(value2)

# to get an integer value between two numbers(inclusive)
# roll of a dice example
value3 = random.randint(1, 6)
print(value3)

# Sample program using randint method, starts here
magic_number = 7
guess = random.randint(1, 10)

if guess == magic_number:
    print("you guessed it right, {}".format(magic_number))
else:
    print("refresh, {}".format(guess))

# ends here

greetings = ["hi", "hello", "hey", "howdy"]
print("{}, Bharath".format(random.choice(greetings)))


# multiple choices

colors = ["green", "blue", "yellow", "white", "orange", "black"]

value4 = random.choices(colors, k=2)
print(value4)

# giving weightage to choices green has 60 out of 160...and respectively for other colors
value5 = random.choices(colors, weights=[60, 30, 20, 10, 10, 30], k=2)
print(value5)

# shuffle
deck = list(range(1, 53))
print("deck of cards\n", deck)

value6 = random.sample(deck, k=20)
print(value6)

# the difference between the sample and the choices is the sample returns a unique list
# where as choices can generate duplicates
# the k value can be bigger than the list when using choices because numbers can be repeated but whereas
# for sample it will throw a 'Value Error'
value7 = random.choices(deck, k=20)
print(value7)
