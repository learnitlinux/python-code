list1 = [10, 3, 44, 0, -1, 9, 7, 22, 38, -4, 6, 45, 32, -19, 22, 110]

print(
    "sorted list with sorted function will create a new list but not store in same list \n",
    sorted(list1),
)
print("list did not changed \n", list1)

list1.sort()
print("here with sort method, it changed the list itself \n", list1)


print("######### REVERSE #############")

print(sorted(list1, reverse=True))
list1.sort(reverse=True)
print(list1)

print("###########tuples#############")
tup = (10, 3, 44, 0, -1, 9, 7, 22, 38, -4, 6, 45, 32, -19, 22, 110)

tup_sorted = sorted(tup)

print(tup_sorted)


print("####### dict ###############")
info = {"Name": "John", "Age": 30, "Salary": 12345}

info1 = sorted(info)

print(info1)

print("############ absolute sort #############")

abcd = [1, 2, 3, 4, -5, -6, -7]
print(sorted(abcd, key=abs))
