print(">>>List Tutorial")
print(list.__doc__)
print("List is an ordered group of elements which can hold a variety of object types")
some_list = ["Sam", 30, 45678.97]
print("Example: ", some_list)
print()
courses = ["CompSci", "Maths", "Physics", "Biology"]
new_courses = ["Chemistry", "BioTech"]


print("Step 1:")
print("courses list:", courses)
print("new_courses list:", new_courses)
print()

print("Step 2")
print("Length of the courses list is", len(courses))  # 4
print("item 2 or index 1 is:", courses[1])  # Maths

# It is easy to work with the last item with -1 rather than using len() function, Biology
print("last item is:", courses[-1])
print()

print("Step 3")
print("Just playing with lists index values to print items of a list")
print(
    "items from 0:2", courses[0:2]
)  # remember the range is always ':' not the "-", ['CompSci', 'Maths']
print(
    "In cases where you don't know the length of the list but want to print last 'n' numbers then use '-n'"
)
print("last 3 items in a list", courses[-3:])  # ['Maths', 'Physics', 'Biology']
print("printing every second item in courses list, using third value", courses[0::2])
print()

# append will add the element to the end of the list, you can also append a list too, ex: is 2 steps below
courses.append("Art")
print("After appending 'Art' in to the list courses list is:\n", courses)
print()

print("Step 4")
courses.insert(
    0, "AI"
)  # insert takes two args, one for the position and another for value
print(courses)
courses_2 = [
    courses,
    "ML",
    "Python",
]  # the courses list is added as list to courses_2, appending without append()
print("course2 here is", courses_2)
print()

print("Step 5")
print(courses)  # just checking how courses looks like before working on it.
courses.insert(0, new_courses)
print(
    "Printing the 'courses' after adding new_courses to '0th' position of 'courses' list)\n",
    courses,
)
print()

print("Step 6")
# courses_3 = courses.insert(0, new_courses) ## gotta check why same command as above, but
# when assigning to a variable is failing
# print(courses_3)
courses_2.extend(new_courses)
# extend will extend the each individual item to the list rather than list as a whole
# I had to check why the below print appended the ['Chemistry', 'BioTech'] in the output
print(
    "Printing the 'courses_2' list after appending it with the 'new_courses' list.\n",
    courses_2,
)
print(
    "Here if you see, every new list we appended will be added to the internal list(if already exist) in the 0th "
    "position."
)
print(courses_2[0][0])
print()

print("Step 7")
print(courses_2)
print("Here we have removed the 'BioTech' element from courses_2")
courses_2.remove("BioTech")
print(courses_2)
print()

print("Step 8")
# Removing an element from a sublist
print(courses_2[0])
courses_2[0].remove("Maths")
print("removed 'Maths' from the above list", courses_2[0])
print()

print("Step 9")
# We can also assign the value to a variable and remove it...
i_hate_biology = "Biology"
courses_2[0].remove(i_hate_biology)
print("I hate {}, so removed it from the list".format(i_hate_biology))
print(courses_2)
print()

print("Step 10")
# ToDo: remove an item which is repeated multiple times in a list(hint: using loops)....
# The below code is not working, I need to check it.
# print(courses_2[0][0])
# print('Chemistry' in courses_2[0][0])
# print(courses_2[0][0].remove('Chemistry'))
# print()

print("popping an element  from a list")
popped = courses_2.pop()
# It will pop off the last value of the list, we can also use an index in pop as pop[2]
print(popped)
print("Observe the list is updated after the pop:\n", courses_2)
print()

print("Step 11")
print(
    "To avoid confusion whether to use 'del' or 'pop', then if you want to use the element after you remove it, use"
    "'pop' else 'del'"
)
print()

print("Step 12")
print(
    "After popping out the 'BioTech' from courses_2 list, printing it in reverse order."
)
courses_2.reverse()
print(courses_2)
print()

print("Step 13")

numbers = [1, 4, 10, -3, -5, 22, 3]
numbers.sort()
# sort the numbers
print(numbers)

numbers = [1, 4, 10, -3, -5, 22, 3]
numbers.sort(reverse=True)
# sort in reverse order
print(numbers)
print()
courses_list = ["Zoology", "Chemistry", "compsci", "maths", "Maths", "Physics"]
sorted_courses = sorted(
    courses_list
)  # sorted is another function will sort the list, but the original list will
# not be affected.


print("Step 14")
print(sorted_courses)
print(courses_list)
print()

print("Step 15")
print("numbers are:", numbers)
print("max value of numbers:", max(numbers))
print("min value of numbers:", min(numbers))
print("sum of numbers:", sum(numbers))
print()

print("Step 16")
print(courses_list.index("maths"))  # it will return the index value
print("Physics" in courses_list)  # True
print()

print("Step 17")
for item in courses_list:
    print(item)

# the enumerate function will print the index value and the item
for index, item in enumerate(courses_list):
    print(index, item)

print()

# to start index as 1 then we can start at

print("Step 18")
for index, item in enumerate(courses_list, start=1):
    print(index, item)

# make the list as a string
coures_str = " - ".join(courses_list)
print()
print("Step 19")
# back to list again
print(coures_str)
print(coures_str.split(" - "))
print()

print("Step 20")
# from basics import random_sample
# print('random', random_sample.choice(courses_list))
# del seems to be simple and useful to delete an item from the list...
# after you remove an element using 'del' statement, you can no longer access that value.
del courses_list[0]
print(courses_list)

print()
print("Step 21")
# range starts from '0' if no  second argument is given.
for value in range(6):
    print("value is:", value)

print()
print("Step 22")
for value in range(10, 16):
    print("value is:", value)

print()

print("Step 23")
print("Storing a range into the list")
num = list(range(1, 6))
print("num list is:", num)
print()

print("Step 24")
print(
    "If we give the third value in range function that would be the step or number to skip"
)
print("To print odd numbers:", list(range(1, 10, 2)))
print("To print even numbers:", list(range(2, 10, 2)))
print("To print numbers step by 4 digits:", list(range(1, 20, 4)))
print()

print("Step 25")
squares = []
for value in range(1, 11):
    squares.append(value**2)

print("To print a square of each number from 1 to 10:", squares)
print()

print("Step 26")
# List comprehensions
print(
    """
List comprehensions allows you to generate this same list in just one line of code, 
the syntax is open a set of square brackets and define the expression for the values
you want to store into the list, then write a for loop to generate the values you want to
feed into the expression. Observe no colon ':' is used at the end of the for loop.

These looks very good in program but tricky to understand if you come back to read it
after a while...\n"""
)
squares = [value**3 for value in range(1, 11) if value != 3]
even_odd = [value if value % 2 == 0 else "ODD" for value in range(1, 11)]
nested_loop = [x * y for x in [1, 2, 3] for y in [10, 100, 1000]]

print("squares:", squares)
print("even or odd:", even_odd)
print("nested_loop", nested_loop)
print()

print(
    """Copying a list to new list needs the [:], if not given then the new name is just a variable to
 original list."""
)
first_list = ["I", "am", "John"]
dup_list = first_list
# Here we are using the slicing concept to make a new copy of list
copy_list = first_list[:]
first_list.append("Matthews")
copy_list.append("Junior")

print("Original list:\n", first_list)
print("Duplicate list generated without [:]\n", dup_list)
print("A new copy of original list which is independent\n", copy_list)
print()

print("Step 27")
# Finding a name in a list irrespective of the upper/lower case..
new_list = ["bharath", "Bharath", "nikhila", "rithvik", "bHaRath", "Bharath"]
# print(new_list)
count = 0
while True:
    if not new_list:
        break
    else:
        popout = new_list.pop()
        if "Bharath" in popout.title():
            # print(new_list)
            count = count + 1

print("Count of any form of word 'bharath' in the above list is: ", count)
print()

print("Step 28")
# Use of arbitrary list of arguments.


def list_names(*names):
    for name in names:
        print("Hello ", name)


list_names("raj", "ramesh", "john")


item = [x + y for x in "cat" for y in "dog"]
item2 = [x + y for x in "cat" for y in "dog" if x != "t" and y != "o"]
item3 = [x + y for x in "cat" for y in "dog" if x != "t" or y != "o"]
print(item)
print(item2)
print(item3)
