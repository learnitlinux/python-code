print("An immutable list is called tuple")
print("Step 1")
tuple_1 = ("History", "Math", "Physics", "CompSci")
tuple_2 = tuple_1

print("tuple_1 is:\n", tuple_1)
print("tuple_2 is copy of tuple_1:\n", tuple_2)
print()

# tuple_1[0] = 'Art' - We get the Error "TypeError: 'tuple' object does not support item assignment"

# # Sets
cs_courses = {"History", "Maths", "Physics", "CompSci"}

print("sets", cs_courses)

set_duplicates = {
    "Maths",
    "Maths",
    "Computers",
    "Biology",
    "Computers",
    "computers",
    "Physics",
}

# it will not print the duplicates
print("set duplicates", set_duplicates)
print("Maths" in set_duplicates)

# courses in common between two lists with 'intersection' method
print(cs_courses.intersection(set_duplicates))
# OR
print(set_duplicates.intersection(cs_courses))

# not common, you see the difference if we switch the set names
print(cs_courses.difference(set_duplicates))
print(set_duplicates.difference(cs_courses))

# union

print(cs_courses.union(set_duplicates))
# OR
print(set_duplicates.union(cs_courses))

# remove an entry
cs_courses.remove("Maths")
print(cs_courses)

# as the set entries are not position bound, the pop value change every time here, make sense!!!
popped = cs_courses.pop()
print(popped)

# examples of creating empty list, tuple, dict or set...
# # Empty Lists
# empty_list = []
# empty_list = list()
#
# # Empty Tuples
# empty_tuple = ()
# empty_tuple = tuple()
#
# # Empty Sets
# empty_set = {} # This isn't right! It's a dict
# empty_set = set()
