print("Python Hisses")

print("John", end=" ")
print("Wick", end="\n\n\n") # ends with multiple newlines

print(
    "The lines are printed",
    "with this message",
    "with 3 dashes separated",
    sep="---",
)
print()

print('Type-1:')
print("     *     " * 3)
print("   *   *   " * 3)
print("  *     *  " * 3)
print(" ********* " * 3)
print("    ***    " * 3)
print("    ***    " * 3)
print("    ***    " * 3)
print()

print('Type-2:')
print("     *     " * 3)
print("   *   *   " * 3)
print("  *     *  " * 3)
print(" ********* " * 3)
print("    ***        ***        ***\n" * 3)

print('Type-3:')
print(
    "     *     " * 3,
    "   *   *   " * 3,
    "  *     *  " * 3,
    " ********* " * 3,
    "    ***        ***        ***\n" * 3,
    sep="\n",
)
