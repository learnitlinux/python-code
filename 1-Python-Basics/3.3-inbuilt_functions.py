# enumerate function.
# the enumerate function will print the index value and the item.
print("Step 1:")
courses_list = ["Zoology", "Chemistry", "compsci", "maths", "Maths", "Physics"]
for index, item in enumerate(courses_list):
    print(index, item)

# zip function.
tuple_b = (1, 2, 3)
tuple_a = ("first", "second", "third")

print("\nzip(a, b) just prints the address in memory:", zip(tuple_a, tuple_b))

for item in zip(tuple_a, tuple_b):
    print(item)
print()

# You can zip as many elements you want, above we just showed only two elements. But it can only expand upto the
# shortest length of all of given lists/tuples.
list1 = [1, 2, 3, 4, 5, 6]
list2 = ["a", "b", "c"]
list3 = ["one", "two", "three", "four"]
for item in zip(list1, list2, list3):
    print(item)
print()

print("Step 2:")
# in operator.
print("x" in ["a", "b", "c"])
print("mykey" in {"yourkey": 123, "theirkey": 456, "mykey": 444}.keys())
print(444 in {"yourkey": 123, "theirkey": 456, "mykey": 444}.values())

# max and min in a list
some_list = [222, 333, 444, 555, 1000]
print("max of a list:", max(some_list))
print("min of a list:", min(some_list))

from random import shuffle, randint

shuffle(some_list)
print(some_list)
rand_integer = randint(123, 456)
print(rand_integer)
