print(
    """Strings are used in Python to record text information, such as names. Strings in Python are actually a 
sequence, which basically means Python keeps track of every element in the string as a sequence. For example, 
Python understands the string "hello' to be a sequence of letters in a specific order. This means we will be able to 
use indexing to grab particular letters (like the first letter, or the last letter)"""
)

print(
    """
1. Are strings mutable?

Strings are not mutable(or) immutable! (meaning you can't use indexing to change individual elements of a string)
means if a string = "abcd" you can't do string[0] = "z"."""
)

mystring = "Hello John, How're you?. are you free to talk?"

print("Step 1")
print(mystring)
print()

print("Step 2")
# we can use the 'end' parameter of print function to put spacing of number of lines or tabs in between
# next print statement.
print(mystring.upper(), end="\n\n\n\n")
print(mystring.lower(), end="\n")
print(mystring.title())
print(mystring.capitalize())
print(mystring.count("are"), end="\t")
print(mystring.count("?"))
print("Is 'John' string in mystring?:", mystring.__contains__("John"))
print("Is 'john' string in mystring?:", mystring.__contains__("john"))
print()

print("Step 3")
print("Lowest index position of string 'John':", mystring.find("John"))
# John starts at index 6 ends at index 9 but in list the end integer is not included, so it gives a negative 1.
# if you replace 9 with 10, it gives value of lower index number which is 6.
print("Finding a string in substring:", mystring.find("John", 4, 9))
print("Finding a string in substring:", mystring.find("John", 4, 10))
if mystring.find("xyz") == -1:
    print("failed")
else:
    print("successful")
print()

print("Step 4")
print(mystring.replace("Hello", "Hi"), end="\n")
newstring = "Shall we have a cup of coffee?"
print(mystring + " " + newstring, end="\n\n")
print()

print("Step 5")
print(dir(mystring), end="\n\n")
# print(help(str))
print(help(str.lower))
print()

print("Step 6")
hello_str = "hello"
print(hello_str.center(11, "*"))
print("Is 'hello' ends with 'abc':", hello_str.endswith("abc"))
print("Is 'hello' ends with ll from index 2 to 4?:", hello_str.endswith("ll", 2, 4))
print("The 'isalnum' method returns false if there are spaces:", mystring.isalnum())
print("Checking the same with hello string:", hello_str.isalnum())
print()

print("Step 7")
string1 = "Python"
string2 = " ".join(string1)
print(string2)
string3 = "www.google.com"
print(string3.split("."))

print("Step 8")
new_string = "This is a just a string in the code"
print(new_string.split("i"))

print()
string_name = "Hello World"
print("Hello World in reverse order:", "{}".format(string_name[::-1]))

my_url = "http://www.JohnWick.com"

print("\nreverse of url: {}".format(my_url[::-1]), end="\n\n")

print("Except last letter:", string_name[:-1])
