# The below coding is segregated to each function and it's operations...

print(
    """Formally, a function is a useful device that groups together a set of statements so they can be run more 
than once. They can also let us specify parameters that can serve as inputs to the functions. We begin with def then 
a space followed by the name of the function. Try to keep names relevant, for example len() is a good name for a 
length() function. Also be careful with names, you wouldn't want to call a function the same name as a built-in 
function in Python (such as len). 

Next come a pair of parentheses with a number of arguments separated by a comma. These arguments are the inputs for 
your function. You'll be able to use these inputs in your function and reference them. After this you put a colon. 

Now here is the important step, you must indent to begin the code inside your function correctly. Python makes use of 
whitespace to organize code. Lots of other programing languages do not do this, so keep that in mind. 

Next you'll see the docstring, this is where you write a basic description of the function. Using iPython and iPython 
Notebooks, you'll be able to read these docstrings by pressing Shift+Tab after a function name. Docstrings are not 
necessary for simple functions, but it's good practice to put them in so you or other people can easily understand 
the code you write. 

After all this you begin writing the code you wish to execute.

A function can deliver multiple print statements, but it will only obey one return.

What is the difference between return and print?
    The return keyword allows you to actually save the result of the 
    output of a function as a variable. The print() function simply displays the output to you, but doesn't save it for 
    future use.

"""
)

# Don't repeat yourself, DRY


def function_name():
    # Try removing the below # and see how it operates
    # global x
    x = 20
    print("'x' value in the function: ", x)


x = 10
print("Step 1")
function_name()
print("'x' value outside the function: ", x)
print()


# positional arguments


def shirt_size_and_message(size, my_message):
    print("My T-shirt size is ", size, "with print saying...", my_message)


print("Step 2")
shirt_size_and_message("Large", "Hello John")
print("positional arguments depends on order of the parameters passed to function def")
shirt_size_and_message("wrong message", "wrong size")

# Key word arguments, below we changed the order but still the message is correct
# due to the fact that we used the keyword arguments
shirt_size_and_message(my_message="Hello Anna", size="Medium")
print()

# default arguments
print("Step 3")


def shirt_with_default_message(size, msg="Hello world"):
    print("My T-shirt size is", size, "with print saying...", msg)


shirt_with_default_message("Medium")
# keyword argument always be appended after the positional arguments as below.
shirt_with_default_message("medium", msg="Hello John")
print()

# Return from a function
print("Step 4")


def return_fullname(first_name, last_name):
    # The below one is the docstring, we can print this too...
    """
    This function will print the fullname, if first_name and last_name were given...
    """
    fullname = "{} {}".format(first_name, last_name)
    return fullname.title()


assigning_to_variable = return_fullname("john", "matthew")
print(assigning_to_variable)
print(return_fullname.__doc__)
print()


# Optional arguments


def fullname_with_optional_middlename(first_name, last_name, middle_name=""):
    if middle_name:
        full_name = "{} {} {}".format(first_name, middle_name, last_name)
    else:
        full_name = "{} {}".format(first_name, last_name)
    return full_name.title()


print("Step 5")
print(fullname_with_optional_middlename("peter", "england"))
print(fullname_with_optional_middlename("johnny", "walker", "junior"))
print()

print("Step 6")


# Returning a dictionary, a function can return more complicated data structures like
# dictionary and lists as well.


def return_dict(first_name, last_name, age, percentage, grade):
    student = {
        "First_Name": first_name,
        "Last_Name": last_name,
        "Age": age,
        "Percentage%": percentage,
        "Grade": grade,
    }
    return student


# std1 = return_dict('bharath', 'jakkani', 33, 77, 10)
# print(std1)
message = "Please Enter to continue your name OR"
message += " type 'quit'"
while True:
    msg = input(message)
    if msg == "quit":
        print("Bye...")
        break
    else:
        first = input("Your first name: ")
        last = input("Your last name: ")
        your_age = input("Your age: ")
        percent = input("Your percentage of marks: ")
        grade_digit = input("You are grade?: ")
        std = return_dict(
            first_name=first,
            last_name=last,
            age=your_age,
            percentage=percent,
            grade=grade_digit,
        )
        print("Student Details\n", std)
        print()
print()

print("Step 7")
# reducing the if else condition to just online with return


def check_dog_string(mystring):
    return "dog" in mystring.lower()


print("Boolean value of method: ", check_dog_string("my dog name is jimmy"))

# passing list as an argument


def print_list_of_names(list_of_names):
    for name in list_of_names:
        print("Hello {}".format(name).title())


names_list = ["Peter", "Sheldon", "Raj"]
print_list_of_names(names_list)
print()

print("Step 8")


# You can pass copy of a list to a function, without altering the original list.
# It's more efficient for a function to work with an existing list to avoid
# using the time and memory needed to make a separate copy, especially when you're
# working with large lists.

# ToDo: the below code is showing some errors need to evaluate...


def sending_messages(messages_list):
    while messages_list:
        pop_value = messages_list.pop()
        print(pop_value)
        sent_messages.append(pop_value)


sent_messages = []
# messages are popped out from last to first, so the messages are in reverse order
messages_list = ["I am fine", "How are you", "Hi Raj"]
# Here we are sending a copy of the list, by using [:] but not the original list, we can print and see
# the original list is still un altered.
# If you remove the [:] the function still works but the original list will be emptied as we have
# used the pop() method in the above function.
sending_messages(messages_list[:])
print("Original messages_list:", messages_list)
print("sent_messages list after appending:", sent_messages)
print()

# Use of Arbitrary number of arguments.
print("Step 9")


def pizza_order(*toppings):
    print("Added below toppings to your PIZZA order:")
    for topping in toppings:
        print(topping)
    print()


# The above syntax works irrespective of how many arguments the function receives.
pizza_order("olives")
pizza_order("mushrooms", "tomatoes")
pizza_order("olives", "mushrooms", "bell peppers")


# ToDo: it is not clear yet, The order of arguments should be, positional arguments, key-word arguments,
#  and then arbitrary arguments.
# The below method uses positional and arbitrary arguments.
print("Step 10")


def employee_info(name, age, company, *skills):
    print("Employee '{}' information:".format(name))
    print(" Age:", age)
    print(" Company:", company)
    print(" Skill set:")
    for skill in skills:
        print("  ", skill)


employee_info("Raj", 25, "Yahoo, Inc", "java", "ruby", "python")
print()

# Using the arbitrary key-word arguments. The **user_info creates an empty dictionary, and pack
# whatever name=value pairs it receives into this dictionary.


def build_user_profile(name, age, **user_info):
    print("User '{}', info:".format(name))
    print("\tAge:", age)
    for key, info in user_info.items():
        print("\t{}: {}".format(key, info))
    # We can return the dictionary as well as below. But assign a variable to funcion calling
    # emp1 = build_user_profile(....) and print emp1 as return below does not have anything to print.
    # return user_info


build_user_profile(
    "John", 25, company="Google, inc", Location="Hyderabad", Department="R&D"
)


# print the given string with lower case letters in odd positions, and upper in even position.


def string_function(args, modified_string=""):
    for index, char in enumerate(args):
        if index % 2 == 0:
            modified_string += char.lower()
        elif index % 2 != 0:
            modified_string += char.upper()
    return modified_string


given_string = "Anthropomorphism"
return_value = string_function(given_string)
print(return_value)
print()

print("Step 11")
employees_hour_list = [
    ("Anita,k", 800),
    ("Billy", 600),
    ("Casper", 600),
    ("John", 500),
    ("Jessy", 300),
    ("Ram", 500),
    ("Anita,p", 800),
]


def find_who_did_max_hours(list_of_employees_with_hours):
    """
    It will return the employee name(s) who have worked the max hours.
    :param list_of_employees_with_hours:
    :return: set of tuples and each tuple has (employee name, hours)
    """
    max_hours = 0
    employee_of_the_month_with_hours = []
    for employee_name, hours in list_of_employees_with_hours:
        if hours >= max_hours:
            if hours > max_hours:
                employee_of_the_month_with_hours.clear()
                employee_of_the_month_with_hours.append((employee_name, hours))
            max_hours = hours
            employee_of_the_month_with_hours.append((employee_name, hours))

    return set(employee_of_the_month_with_hours)


result = list(find_who_did_max_hours(employees_hour_list))
print(result)
