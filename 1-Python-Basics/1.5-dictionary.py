# dictionary as an example format string
print("Step 1:")
print(
    """
Dictionaries are an unordered mappings for storing objects, unlike lists which are ordered sequence,
dictionaries are key, value pairs. A user can quickly grab the objects without needing to know the
index location.

Dictionaries are mutable like lists.\n"""
)

person = {"name": "John", "age": 30, "skills": ["Linux", "Unix", "Python"]}

print("My name is {}, I am {} years old".format(person["name"], person["age"]))
print(
    "My name is {0}, I am {1} years old, My full name is {0} Wick".format(
        person["name"], person["age"]
    )
)
string = "My Name is {} and age is {}".format(person["name"], person["age"])
print("string is:- ", string)

print("########un packing the dictionary, with mutliple lines ###############")
print(
    """My name is {name}, I am {age} years old. My full name is {name} Wick.
My skill sets are {skills}""".format(
        **person
    )
)

student = {"Name": "John", "Age": 25, "Courses": ["Maths", "Physics"]}

print("\nStep 1.1:")
print("Student dictionary:\n", student)
print("Student courses list:", student["Courses"])

# prints True
print("Is Maths is in student's courses list?:", "Maths" in student["Courses"])
print()

print("Step 2:")
student["Grade"] = "10th"
student["Section"] = "B"
print(student)
print()

print("Step 3:")
# print(student['phone']) - this will give a KeyError: 'phone', so we use the get method to see if exists?
print(student.get("Phone"))  # will return none as the phone key does not exist
print(student.get("Phone", "Key not found, please check it"))
print()

print("Step 4:")
student["Fees"] = 100
# Arithmetic operations on a dictionary value
print(2 * student["Fees"])
print()

print("Step 5:")
# pop method will take out the key from a dictionary
print("Before poping the 'Name' key out\n", student)
student_name = student.pop("Name")
print("After poping the 'Name' key out\n", student)  # without name key:value pair
print(student_name)
print()

print("Step 6:")
student.update({"Std_ID": "JohnW10B23"})  # remember the {} brackets
print(student)
print()

print("Step 7:")
del student["Age"]
print("deleted student 'Age' key from dictionary\n", student)
print("Length of student dictionary is:", len(student))
print()

print("Step 8:")
print(student.keys())
print(student.values())
print(student.items())
print()

# Looping through dictionary
print("Step 9:")
for key, value in student.items():
    print(key, "--", value)
print()

print("Looping through 'keys' only (in student dictionary)")

# The default behaviour when you loop through a dictionary is the looping through keys.
# Either below this
# for key in student:
# Or use below

for key in student.keys():
    print(key.upper())
print()

print("looping through 'values' only (in student dictionary)")
for val in student.values():
    print(val)
print()

print("Step 10:")
print("Sorted keys from student dictionary, using the sorted() function")
for key in sorted(student.keys()):
    print(key.title())
print()
duplicate_dict = {
    "language_1": "c",
    "language_2": "python",
    "language_3": "c",
    "language_4": "ruby",
    "language_5": "python",
}

print("Looping through unique 'values' from a dictionary using set()")

for val in set(duplicate_dict.values()):
    print(val.title())
print()

print("Step 11:")

"""
Below we are modifying the values of a dict key:value pair with the if clause
"""
car = {
    "Name": "BMW",
    "Speed": "fast",
    "Color": "Red",
    "Distance_travelled": 100,
    "Measure": "km",
}
dist = car["Distance_travelled"]
if car["Speed"] == "slow":
    dist = dist + 20
elif car["Speed"] == "medium":
    dist = dist + 50
else:
    dist = dist + 100

car["Distance_travelled"] = dist

print(
    "Distance travelled with {} speed is {}{}".format(
        car["Speed"], car["Distance_travelled"], car["Measure"]
    )
)
print("car details:\n", car)
print()

print("Step 12:")
# Nesting
"""
Sometimes we we want to store multiple dictionaries in a list, or a list of items as a
value in a dictionary.
"""
# The below example shows how we can take a list of items from the names and ids list
# and make a dictionary for each of those two items.

std_list = []
std_names = [
    "ram",
    "john",
    "joseph",
    "kathy",
    "adam",
    "krishna",
    "mike",
    "anitha",
    "rupa",
    "Ramu",
]
std_id = list(range(145, 155))

# Created two lists of 10 entries each and using them to form a dictionary.
for each in range(10):
    new_std = {"name": std_names[each].title(), "std_id": std_id[each]}
    std_list.append(new_std)

print(std_list)
print("Length of a student list of dictionary:", len(std_list))
print()

print("Step 13:")
# Dictionaries inside a list
emp_1 = {"Name": "Ram", "ID": 123, "City": "Hyderabad"}
emp_2 = {"Name": "Rajesh", "ID": 345, "City": "Mumbai"}
emp_3 = {"Name": "Mike", "ID": 456, "City": "Newyork"}

emp_list = [emp_1, emp_2, emp_3]

for each_emp in emp_list:
    for key, val in each_emp.items():
        print(key.title(), "-", val)
    print()
print()

print("Step 14:")
print("Create a dictionary from two tuples as key, value pairs")
tuple_b = (1, 2, 3, 4, 5)
tuple_a = ("first", "second", "third", "fourth", "fifth")

dict_ab = dict(zip(tuple_a, tuple_b))
print(dict_ab)

# We can merge two dictionaries with the update() method, but it will replace the vlaues if the Keys are matched.
# see below, the emp_1 values were replaced by emp_2 because the keys are same.
emp_1.update(emp_2)
print(emp_1)
print(emp_1 == emp_2)
emp_1.update(dict_ab)
print(emp_1)
print()

print("Step 15:")

print(emp_1)
emp_1.pop("third")
print("\nAfter pop() method:\n", emp_1)

# popitem() method pops last item.
emp_1.popitem()
print("\nAfter popitem() method.:\n", emp_1)

emp_1.setdefault("Salary", 20000)
print("Default Salary of employee emp_1:", emp_1.get("Salary"))
print(emp_1)

emp_1.clear()
print("\nemp_1 dictionary has been emptied with clear() method.:", emp_1)

# ToDo:
# Dictionary inside a dictionary
# all(), bool(), asciii(), any()
