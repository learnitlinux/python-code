# Comparisons:
# Equal:            ==
# Not Equal:        !=
# Greater Than:     >
# Less Than:        <
# Greater or Equal: >=
# Less or Equal:    <=
# Object Identity:  is


# and
# or
# not

user = "John"
# password = True
password = False

print("Step 1:")
if user == "John" and password == True:
    print("John has permissions to login")
else:
    print("John 'Do Not' have permission to login")

fruit = "apple"
phone = "apple"

if fruit == "apple" or phone == "apple":
    print("Both the 'fruit' and 'phone' are Apples")
else:
    print("Neither fruit nor phone is an Apple")

logged_in = False
user = "John"

if not logged_in:
    print("{}, Please login".format(user))
else:
    print("{}, you are already logged in".format(user))
print()

a = [1, 2, 3]
b = [1, 2, 3]
c = [4, 5, 6]
d = c

print("Step 2:")
print("List 'a' and 'b' are equal 'a == b': ", a == b)
# these are two different objects in memory, we can print out these locations with the builtin id function
print("List 'a' and 'b' are same 'a is b': ", a is b)
print("List 'c' and 'd' are equal 'c == d': ", c == d)
print("List 'c' and 'd' are same 'c is d': ", c is d)
print("ID of a:", id(a))
print("ID of b:", id(b))
print("ID of c:", id(c))
print("ID of d:", id(d))
print("id(a) == id(b):", id(a) == id(b))
print("id(c) == id(d)", id(c) == id(d))
print()
# False Values:
#     False
#     None
#     Zero of any numeric type
#     Any empty sequence. For example, ''--this is empty string, (), [].
#     Any empty mapping. For example, {}.

print("Step 3:")
# condition = False
# condition = None
# condition = 0
# condition = ()
condition = {}

if condition:
    print("'condition' var is empty:", not condition)
else:
    print("condition' var is empty:", condition)

location = "Canada"
if location == "USA":
    print("You are in USA")
elif location == "Germany":
    print("You are in Germany")
elif location == "Canada":
    print("You are in Canada")
else:
    print("You are in India by default")
