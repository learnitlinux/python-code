from basics import fibonacci, fibonacci as fb

fibonacci.fibo(100)

# second example with 'as'

fb.fibo(200)

# third example where importing a method from module-builtin with 'as'
from basics.fibonacci import fibo as fb2

fb2(300)
