import logging

print(
    """
Problem Statement: Return small if both numbers are even else returns the greater one, if one or both numbers are odd."""
)


def return_small_num_of_two_even_or_big_if_one_odd(a=0, b=0):
    if (a % 2 == 0) and (b % 2 == 0):
        if a < b:
            print("Both given numbers are even, small even number is: {}".format(a))
        else:
            print("Both given numbers are even, small even number is: {}".format(b))
    else:
        if a > b:
            print("One or both given numbers are odd, big number is: {}".format(a))
        else:
            print("One or both given numbers are odd, big number is: {}".format(b))


return_small_num_of_two_even_or_big_if_one_odd(1244, 1104)
return_small_num_of_two_even_or_big_if_one_odd(55, 200)
return_small_num_of_two_even_or_big_if_one_odd(455, 777)

print()
#######################################################################

print(
    """
Write a function takes a two-word string and returns True if both words begin with same letter.
"""
)


def check_two_string_has_same_start_letter(my_string):
    """
    Checks if the first letters of two word strings has same letter or not!
    :param my_string: e.g: 'hello world'
    :return: True or False
    """
    first_char_list = []
    split_strings = my_string.split(sep=" ")
    if len(split_strings) == 2:
        # THE BELOW IS MY IDEA WHICH IS MORE COMPLEX AND FROM THE AUTHOR OF UDEMY I FOUND THE SIMPLE SOLUTION.
        # https://github.com/Pierian-Data/Complete-Python-3-Bootcamp/blob/master/03-Methods%20and%20Functions/04-Function%20Practice%20Exercises%20-%20Solutions.ipynb
        # # find and append first character of two strings and break the loop
        # for each_sub_string in split_strings:
        #     for index, char in enumerate(each_sub_string):
        #         first_char_list += char
        #         break
        # if first_char_list[0].lower() == first_char_list[1].lower():
        #     return True
        # else:
        #     return False
        return split_strings[0][0].lower() == split_strings[1][0].lower()
    else:
        return "Input Error"


print(check_two_string_has_same_start_letter("lovely Lamb"))
print(check_two_string_has_same_start_letter("Spicy chicken"))
print(check_two_string_has_same_start_letter("hello world there"))
print()
#######################################################################

print("Check for '20'")


def checks_for_twenty(num1, num2):
    """
    Returns True if one of two given numbers is 20 or sum is 20 else false
    :return: True or False
    """
    my_list = [num1, num2]
    if 20 in my_list or num1 + num2 == 20:
        return True
    else:
        return False


print("20, 10:", checks_for_twenty(20, 10))
print("12, 8:", checks_for_twenty(12, 8))
print("2, 3:", checks_for_twenty(2, 3))
print()
#######################################################################
print("Capitalize the first and fourth letter of a string.")

# My below logic has issue as it is updating all the matching chars instead of positioned at index values 0 and 3.
# def old_macdonald(name_string):
#     for index, letter in enumerate(name_string):
#         if index == 0 or index == 3:
#             name_string = name_string.replace(letter, letter.upper())
#     print(name_string)
#
#
# old_macdonald("fooname")
# old_macdonald("macdonalddddd")


def old_donald(given_name_string):
    if len(given_name_string) > 3:
        given_name_string = (
            given_name_string[:3].capitalize() + given_name_string[3:].capitalize()
        )
        print(given_name_string)


old_donald("macdonald")
old_donald("macdonaldddd")
print()
###############################################################
print("Return a sentence with the words reversed.")


def reverse_the_words(my_string):
    my_string_splitted_into_list = my_string.split(" ")
    if len(my_string_splitted_into_list) > 2:
        print(" ".join(my_string.split()[::-1]))
    else:
        logging.error(
            "Minimum two words string needed, but only given: '{}'".format(my_string)
        )


reverse_the_words("Hello World I am Here")
reverse_the_words("new")
print()
###################################################

print("Almost there method")


def almost_there(given_number):
    # From the the Udemy author it is much simpler than my code
    # return ((abs(100 - n) <= 10) or (abs(200 - n) <= 10))
    # https://github.com/Pierian-Data/Complete-Python-3-Bootcamp/blob/master/03-Methods%20and%20Functions/04-Function%20Practice%20Exercises%20-%20Solutions.ipynb
    if given_number in range(90, 111):
        print("Success")
    elif given_number in range(190, 211):
        print("Success")
    else:
        # logging.error("Not in the range 10 +- to 100 or 200, given number '{}'".format(given_number))
        print(
            "Not in the range 10 +- to 100 or 200, given number '{}'".format(
                given_number
            )
        )


# Check
almost_there(104)
# Check
almost_there(110)
# Check
almost_there(150)
# Check
almost_there(209)
print()
#####################################################

print("Whether an array has 3 next to 3 ")


def has_33_or_not(given_list):
    # my_threes_list = []
    # for index, value in enumerate(given_list):
    #     if value == 3:
    #         my_threes_list.append(value)
    #         try:
    #             if given_list[index+1] == 3:
    #                 my_threes_list.append(value)
    #                 break
    #             else:
    #                 my_threes_list = []
    #         except Exception:
    #             continue
    # return my_threes_list == [3, 3]

    # Author has a simpler code
    for i in range(0, len(given_list) - 1):
        if given_list[i] == 3 and given_list[i + 1] == 3:
            return True
    return False

    # Check


print("1:", has_33_or_not([1, 3, 3, 1]))
# Check
print("2:", has_33_or_not([1, 3, 1, 3]))
# Check
print("3:", has_33_or_not([3, 1, 3]))
print()

print(
    "Paper Doll: : Given a string, return a string where for every character in the original there are three characters"
)


def paper_doll(given_string):
    new_string = ""
    for each in given_string:
        new_string += "".join(each * 3)
        # or
        # new_string += each*3

    return new_string


# Check
print("1:", paper_doll("Hello"))
# Check
print("2:", paper_doll("Mississippi"))
print()

print(
    "BLACKJACK: Given three integers between 1 and 11, if their sum is less than or equal to 21, "
    "return their sum. If their sum exceeds 21 and there's an eleven, reduce the total sum by 10. Finally, "
    "if the sum (even after adjustment) exceeds 21, return 'BUST'"
)


def blackjack2(a, b, c):
    if sum((a, b, c)) <= 21:
        return sum((a, b, c))
    elif sum((a, b, c)) <= 31 and 11 in (a, b, c):
        return sum((a, b, c)) - 10
    else:
        return "BUST"


print("1:", blackjack2(5, 6, 7))
print("2:", blackjack2(9, 9, 9))
print("3:", blackjack2(9, 9, 11))
print("4:", blackjack2(12, 9, 10))
print("5:", blackjack2(10, 10, 11))
print()


def if_it_is_aprime_no(num):
    count = 0
    for each_num in range(1, num + 1):
        if num % each_num == 0:
            count += 1
    if count == 2:
        return True
    else:
        return False


print()

print(
    "COUNT PRIMES: Write a function that returns the number of prime numbers that exist up to and including a given number"
)


def count_prime_no_upto(num):
    prime_count = 0
    prime_list = []
    for each in range(1, num + 1):
        if if_it_is_aprime_no(each):
            prime_list.append(each)
            prime_count += 1
    return prime_list, prime_count


print("1:", count_prime_no_upto(100))
print("2:", count_prime_no_upto(0))
print("3:", count_prime_no_upto(1))
print("4:", count_prime_no_upto(2))
print("5:", count_prime_no_upto(7))
