# Program to allot table in a restaurant for guests less than 10, else ask to wait.
prompt_message = "Hello sir/madam, "
prompt_message += "How many of you for tonight dinner?: "

message = input(prompt_message)
count = int(message)
if count > 10:
    print("Please wait for 10 minutes for the table!")
else:
    print("Please come this way!, we have a table ready for you :)")

# Program for an odd or even number
prompt_msg = "Let's see if number you entered is odd or even, please enter a number or type 'quit': "
# Here we are using a flag named 'active' to set it to True or False like a signal to run a program
# or quit
active = True
while active:
    msg = input(prompt_msg)
    if msg == "quit":
        active = False
        print("Exited")
        break
    else:
        num = int(msg)
        if num % 2 == 0:
            print("It's an even number")
        else:
            print("It's an odd number")


# Multiples of Ten program
message = "Let's see the number you entered is multiples of 10, please enter a valid number or 'quit': "
active = True
while active:
    msg = input(message)
    if msg == "quit":
        active = False
        print("Exited")
        break
    else:
        num = int(msg)
        if num % 10 == 0:
            print("The number is a multiple of 10")
        else:
            print("It is not divisible by 10")
