print("Step 1:")
print("###### filter function definition #####")
print(
    "The filter function returns an iterator yielding those items of iterable for which function(item) is true. "
    "Meaning you need to filter by a function that returns either True or False. Then passing that into filter ("
    "along with your iterable) and you will get back only the results that would return True when passed to the "
    "function.\n"
)
print(filter.__doc__)
print("See 'None' example below:")

list_of_items = [1, None, False, 4]
print(list(filter(None, list_of_items)))

list_of_numbers = [2, 4, 7, 8]


def even_odd(num):
    return num % 2 == 0


print("Results with a defined function:", list(filter(even_odd, list_of_numbers)))
print(
    "Results with lambda function:", list(filter(lambda x: x % 2 == 0, list_of_numbers))
)

names_list = ["Andy", "Rosa", "John", "Ana"]
print("Even string: ", list(filter(lambda x: len(x) % 2 != 0, names_list)))
