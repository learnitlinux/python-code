print("Step 1:")
print("#### Map function definition ####")
print(
    """The map function allows you to 'map' a function to an iterable object. That is to say you can quickly call the "
      "same function to every item in an iterable, such as a list.
      Also, note that when we pass a function to map we don't mention the parenthesis '()'"""
)
print(map.__doc__)
print()
list_of_num = [2, 4, 6]


def cube_of_a_num(elem):
    return elem**3


print("Step 1:")
# If we have to get the outcome of the compute as a list, use list(). But just printing the map() will result the memory
# location itself.
print(map(cube_of_a_num, list_of_num))
print("Results with a defined function:", list(map(cube_of_a_num, list_of_num)))

# Instead of a small function "cube_of_a_num", we can use the lambda function
print("Step 2:")
print("Results with lambda function:", list(map(lambda x: x**3, list_of_num)))


# Reverse given names lambda expression

names_list = ["Andy", "Rosa", "John", "Ana"]
print("Reverse names:", list(map(lambda x: x[::-1], names_list)))

# Example of lambda function.
my_square = lambda num: num**2
print(my_square(3))
