print(
    """
    Python lambda functions are anonymous, single use, throw away functions. They are just for short term and, 
    temporary use.
    Syntax is: lambda arguments: expression
    Here, expression is the return value.
    lambda's body is a single expression, not a block of statements.
    """
)

print("Step 1:")

add5 = lambda x: x + 5
print("Add 5 to a number:", add5(10))

math_function = lambda a, b, c, d: (a + b) * c + d
print("Result of math function:", math_function(2, 3, 2, 10))

square_of_num = lambda x: x * x
print("Square of a given number", square_of_num(9))

get_tens = lambda p: int(p / 10) % 10
print("Gets the digit in tenth place of a given number:", get_tens(45978.12))

fourth_letter = lambda x: x[3]
last_letter = lambda x: x[-1]
reverse_string = lambda x: x[::-1]
print("Gets the fourth letter of a string: '{}'".format(fourth_letter("Hello John")))
print("Gets the last letter of a string: '{}'".format(last_letter("Hello Wick")))
print("Reverse a string: '{}'".format(reverse_string("nam norI")))


def outer_function(second_string):
    return lambda x: x + " " + second_string


initiate_method = outer_function("Wick")
print(initiate_method("John"))
print(initiate_method("Adam"))
print()

print("Step 2:")
# We can use lambda functions as a argument in other functions and that is most common use.
list1 = [("eggs", 10), ("carrots", 7), ("honey", 8), ("apple", 15)]
print("Sorting by keys: ")
list1.sort(key=lambda x: x[0])
print(list1)
print("Sorting by values: ")
list1.sort(key=lambda x: x[1])
print(list1)
print()

print("Step 3:")
list_new = [
    {"make": "Tesla", "model": "Xclass", "year": 2019},
    {"make": "Ford", "model": "Focus", "year": 2018},
    {"make": "Merc", "model": "Eclass", "year": 2017},
    {"make": "Porsche", "model": "taycan", "year": 2020},
]

print("Sorted by make of the year:")
list_new_sorted = sorted(list_new, key=lambda x: x["year"])
print(list_new_sorted)

print("Sorted by manufacturer:")
list_new_sorted = sorted(list_new, key=lambda y: y["make"])
print(list_new_sorted)
print()

print("Step 4:")
list_num = list(range(1, 21))
print(list_num)

list_even = list(filter(lambda x: x % 2 == 0, list_num))
print(list_even)
