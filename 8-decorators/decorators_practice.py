# Example of calling a function that is inside an another function.


def my_main_func():
    def sub_func():
        return "sub_func"

    return "main_func + {}".format(sub_func())


new_func_def = my_main_func()
print(new_func_def)

# Passing a function as an argument into another function.


def my_hello(name=""):
    return "Hi {}".format(name)


def greet(my_hello):
    print(my_hello)


greet(my_hello("Bharath"))


# Instead of passing a function as an argument to other function we can use decorators.
print("Use of decorators:")


def meet_n_greet(my_hello):
    print("first")
    print(my_hello())
    print("last")


@meet_n_greet
def new_hello():
    return "Hi John"
